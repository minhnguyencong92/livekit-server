package routes

import (
	"app/interfaces"
	"app/modules/call"
	"app/modules/cdn"
	"app/modules/device"
	"app/modules/notify"
	"app/modules/oauth"
	"app/modules/static"
	"app/modules/ws"

	"github.com/gofiber/fiber/v2"
)

func V1(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) *fiber.App {
	r := fiber.New(fiber.Config{ErrorHandler: lib.Middleware.Error()})

	// admin := r.Group("/admin")
	// admin.Mount("/account", account.NewRouterAdmin(rpo, lib, srv))
	// admin.Mount("/group", group.NewRouterAdmin(rpo, lib, srv))
	// admin.Mount("/role", role.NewRouterAdmin(rpo, lib, srv))

	all := r.Group("/")
	all.Mount("/cdn", cdn.NewRouter(rpo, lib, srv))
	all.Mount("/device", device.NewRouter(rpo, lib, srv))
	all.Mount("/notify", notify.NewRouter(rpo, lib, srv))
	all.Mount("/oauth", oauth.NewRouter(rpo, lib, srv))
	all.Mount("/static", static.NewRouter(rpo, lib, srv))
	all.Mount("/ws", ws.NewRouter(rpo, lib, srv))
	all.Mount("/call", call.NewRouter(rpo, lib, srv))

	return r
}
