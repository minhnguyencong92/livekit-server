package user

import (
	"app/interfaces"
	"app/models"
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type repo struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
}

func NewRepo(rpo *interfaces.IRpo, lib *interfaces.ILib) interfaces.IRpoUser {
	return repo{rpo: rpo, lib: lib}
}

func (o repo) Create(domain *models.UserDomain) error {
	ctx := context.TODO()

	obj, err := o.lib.Db.User().InsertOne(ctx, domain)
	if err != nil {
		return err
	}

	domain.ID = obj.InsertedID.(primitive.ObjectID)
	return nil
}

func (o repo) Update(domain *models.UserDomain) error {
	ctx := context.TODO()

	_, err := o.lib.Db.User().UpdateOne(ctx, models.M{"_id": domain.ID}, models.M{"$set": domain})
	if err != nil {
		return err
	}

	return nil
}

func (o repo) FindAll(req *models.Req) ([]*models.UserDomain, error) {
	ctx := context.TODO()
	res := []*models.UserDomain{}

	filter := models.M{}
	if len(req.Filters) > 0 {
		filter = models.M{"$and": req.Filters}
	}

	cursor, err := o.lib.Db.User().Find(ctx, filter, options.Find().SetSkip(req.Skip).SetLimit(req.Limit).SetSort(req.Sort))
	if err != nil {
		return nil, err
	}

	defer cursor.Close(ctx)
	if err := cursor.All(ctx, &res); err != nil {
		return nil, err
	}

	return res, nil
}

func (o repo) Count(req *models.Req) (int64, error) {
	ctx := context.TODO()

	filter := models.M{}
	if len(req.Filters) > 0 {
		filter = models.M{"$and": req.Filters}
	}

	return o.lib.Db.User().CountDocuments(ctx, filter)
}

func (o repo) FindById(id string) (*models.UserDomain, error) {
	ctx := context.TODO()
	res := &models.UserDomain{}
	oid, _ := primitive.ObjectIDFromHex(id)

	result := o.lib.Db.User().FindOne(ctx, models.M{"_id": oid})
	if result.Err() != nil {
		return nil, result.Err()
	}

	result.Decode(&res)
	return res, nil
}

func (o repo) FindByUsername(username string) (*models.UserDomain, error) {
	ctx := context.TODO()
	res := &models.UserDomain{}

	result := o.lib.Db.User().FindOne(ctx, models.M{"username": username})
	if result.Err() != nil {
		return nil, result.Err()
	}
	result.Decode(&res)
	return res, nil
}

func (o repo) UpsertByUsername(domain *models.UserDomain) error {
	ctx := context.TODO()
	opts := options.Update().SetUpsert(true)

	obj, err := o.lib.Db.User().UpdateOne(ctx, models.M{"username": domain.Username}, models.M{"$set": domain}, opts)
	if err != nil {
		return err
	}

	if obj.UpsertedID != nil {
		domain.ID = obj.UpsertedID.(primitive.ObjectID)
	}

	return nil
}
