package call

import (
	"app/clog"
	"app/enums"
	"app/interfaces"
	"app/models"
	"net/http"

	"github.com/gofiber/adaptor/v2"
	"github.com/gofiber/fiber/v2"
	"github.com/livekit/protocol/auth"
	"github.com/livekit/protocol/webhook"
)

type ctr struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	srv *interfaces.ISrv
}

func NewRouter(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) *fiber.App {
	c := ctr{rpo: rpo, lib: lib, srv: srv}
	r := fiber.New(fiber.Config{ErrorHandler: lib.Middleware.Error()})

	r.Post("/room/join", c.GetToken())
	r.Post("/room/create", c.MakeCall())
	r.Get("/room/list", c.ListRooms())
	r.Get("/participant/list/:room", c.ListParticipants())
	r.Delete("/room/leave/:name", c.LeaveRoom())
	r.Post("/webhook", adaptor.HTTPHandlerFunc(c.HandlerWebhook))

	return r
}

func (o ctr) GetToken() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		data := models.TokenJoinRoomRequest{}

		if res := o.lib.Middleware.Validate(c, &data); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		result := o.srv.Call.GetToken(&req, data.RoomName, data.Identity)

		return c.Status(result.Status).JSON(result)
	}
}

func (o ctr) HandlerWebhook(w http.ResponseWriter, r *http.Request) {
	authProvider := auth.NewSimpleKeyProvider(
		enums.Env.LiveKitKey, enums.Env.LiveKitSecret,
	)
	event, err := webhook.ReceiveWebhookEvent(r, authProvider)
	if err != nil {
		clog.Error(err)
	}
	clog.Info(event)
}

func (o ctr) MakeCall() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		data := models.CreateRoomRequest{}

		if res := o.lib.Middleware.Validate(c, &data); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		result := o.srv.Call.MakeCall(&req, data.Identity, data.RoomName)
		return c.Status(result.Status).JSON(result)
	}
}

func (o ctr) ListRooms() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := o.srv.Call.ListRoom(&req)
		return c.Status(res.Status).JSON(res)
	}
}

func (o ctr) ListParticipants() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := o.srv.Call.ListParticipants(&req, c.Params("room"))
		return c.Status(res.Status).JSON(res)
	}
}

func (o ctr) LeaveRoom() fiber.Handler {
	return func(c *fiber.Ctx) error {
		// viet chua xong
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := o.srv.Call.LeaveRoom(&req, c.Params("name"), "")

		return c.Status(res.Status).JSON(res)
	}
}
