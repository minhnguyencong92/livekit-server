package call

import (
	"app/enums"
	"app/interfaces"
	"app/models"
	"net/http"

	"github.com/google/uuid"
)

type service struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	srv *interfaces.ISrv
}

func NewService(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) interfaces.ISrvCall {
	return service{rpo: rpo, lib: lib, srv: srv}
}

func (o service) MakeCall(req *models.Req, identity string, roomName string) *models.Res {
	// roomName := uuid.NewString()
	// identity := time.Now().String()
	resRoom := o.createRoom(req, roomName)
	if resRoom.Status != http.StatusOK {
		return resRoom
	}

	callRoom := resRoom.Data.(models.CallRoom)

	resToken := o.getToken(req, callRoom.RoomName, identity)
	if resToken.Status != http.StatusOK {
		return resToken
	}
	callToken := resToken.Data.(models.CallToken)

	res := &models.Res{CodeMsg: enums.Code.Success[req.Lang]}
	res.Data = models.MakeCall{Room: callRoom, Token: callToken.Token}
	return res

}

func (o service) GetToken(req *models.Req, roomName string, identity string) *models.Res {
	return o.getToken(req, roomName, identity)
}

func (o service) getToken(req *models.Req, roomName string, identity string) *models.Res {
	token, err := o.lib.Call.TokenJoinGroup(roomName, identity)
	if err != nil {
		return &models.Res{CodeMsg: enums.Code.Error[req.Lang], Error: err.Error()}
	}
	return &models.Res{CodeMsg: enums.Code.Success[req.Lang], Data: models.CallToken{Token: token}}
}

func (o service) CreateRoom(req *models.Req, roomName string) *models.Res {
	return o.createRoom(req, roomName)
}

func (o service) createRoom(req *models.Req, roomName string) *models.Res {
	// existRoom, err := o.lib.Call.GetRoom(roomName)
	// if err != nil {
	// 	return &models.Res{CodeMsg: enums.Code.RoomNotFound[req.Lang], Error: err.Error()}
	// }

	// if existRoom != nil {
	// 	log.Println("Room", roomName, "Exist")
	// 	return &models.Res{CodeMsg: enums.Code.RoomExists[req.Lang]}
	// }

	if roomName == "" {
		roomName = uuid.NewString()
	}

	room, err := o.lib.Call.CreateRoom(roomName)
	for err != nil {
		roomName = uuid.NewString()
		room, err = o.lib.Call.CreateRoom(roomName)
	}
	// if err != nil {
	// 	return &models.Res{CodeMsg: enums.Code.Error[req.Lang], Error: err.Error()}
	// }

	return &models.Res{CodeMsg: enums.Code.Success[req.Lang], Data: models.CallRoom{RoomName: roomName, RoomID: room.Sid}}

}

func (o service) ListRoom(req *models.Req) *models.Res {
	rooms, err := o.lib.Call.ListRooms()
	if err != nil {
		return &models.Res{CodeMsg: enums.Code.Error[req.Lang], Error: err.Error()}
	}

	res := []models.CallRoom{}
	for _, item := range rooms.Rooms {
		res = append(res, models.CallRoom{RoomID: item.Sid, RoomName: item.Name})
	}

	return &models.Res{CodeMsg: enums.Code.Success[req.Lang], Data: res}
}

func (o service) ListParticipants(req *models.Req, roomName string) *models.Res {
	participants, err := o.lib.Call.ListParticipantsInRoom(roomName)
	if err != nil {
		return &models.Res{CodeMsg: enums.Code.Error[req.Lang], Error: err.Error()}
	}

	res := []models.CallParticipant{}
	for _, item := range participants.Participants {
		res = append(res, models.CallParticipant{ID: item.Sid, Name: item.Name, Identity: item.Identity})
	}

	return &models.Res{CodeMsg: enums.Code.Success[req.Lang], Data: res}
}

func (o service) LeaveRoom(req *models.Req, roomName string, identity string) *models.Res {
	_, errRemoveParticipant := o.lib.Call.RemoveParticipant(roomName, identity)

	if errRemoveParticipant != nil {
		return &models.Res{CodeMsg: enums.Code.RoomNotFound[req.Lang], Error: errRemoveParticipant.Error()}
	}

	existRoom, err := o.lib.Call.GetRoom(roomName)
	if err != nil || existRoom == nil {
		return &models.Res{CodeMsg: enums.Code.RoomNotFound[req.Lang], Error: err.Error()}
	}

	if existRoom.NumParticipants == 0 {
		o.lib.Call.DeleteRoom(roomName)
	}

	return &models.Res{CodeMsg: enums.Code.Success[req.Lang]}
}
