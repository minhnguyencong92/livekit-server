package token

import (
	"app/interfaces"
	"app/models"
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type repo struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
}

func NewRepo(rpo *interfaces.IRpo, lib *interfaces.ILib) interfaces.IRpoToken {
	return repo{rpo: rpo, lib: lib}
}

func (o repo) Create(domain *models.TokenDomain) error {
	ctx := context.TODO()

	filter := models.M{"accessToken": domain.AccessToken}
	if domain.User == "" {
		oid, _ := primitive.ObjectIDFromHex(domain.Client)
		filter = models.M{"_id": oid}
		domain.ID = oid
		domain.AccessToken = fmt.Sprintf("%v-%v", domain.Client, domain.AccessToken)
		o.lib.Rdb.Dels(domain.Client)
	}

	o.lib.Db.Token().DeleteMany(ctx, models.M{"expiresAt": models.M{"$lt": time.Now()}})

	obj, err := o.lib.Db.Token().UpdateOne(ctx, filter, models.M{"$set": domain}, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}

	if obj.UpsertedID != nil {
		domain.ID = obj.UpsertedID.(primitive.ObjectID)
	}

	return nil
}

func (o repo) Update(domain *models.TokenDomain) error {
	ctx := context.TODO()

	_, err := o.lib.Db.Token().UpdateOne(ctx, models.M{"_id": domain.ID}, models.M{"$set": domain})
	if err != nil {
		return err
	}

	return nil
}

func (o repo) Delete(data *models.TokenData) error {
	ctx := context.TODO()
	filter := models.M{"accessToken": data.AccessToken}

	if data.UserId != "" {
		filter = models.M{"user": data.UserId}
	}

	list, err := o.lib.Db.Token().Distinct(ctx, "accessToken", filter)
	if err != nil {
		return err
	}

	tokens := []string{}
	for _, v := range list {
		tokens = append(tokens, v.(string))
	}
	o.lib.Rdb.Del(tokens...)

	if data.IsDeleteDB {
		o.lib.Db.Token().DeleteMany(ctx, models.M{"accessToken": models.M{"$in": list}})
	}

	return nil
}

func (o repo) FindByAccessToken(accessToken string) (*models.TokenDomain, error) {
	ctx := context.TODO()
	res := &models.TokenDomain{}

	result := o.lib.Db.Token().FindOne(ctx, models.M{"accessToken": accessToken})
	if result.Err() != nil {
		return nil, result.Err()
	}

	result.Decode(&res)
	return res, nil
}
