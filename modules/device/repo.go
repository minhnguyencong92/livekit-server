package device

import (
	"app/interfaces"
	"app/models"
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type repo struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
}

func NewRepo(rpo *interfaces.IRpo, lib *interfaces.ILib) interfaces.IRpoDevice {
	return repo{rpo: rpo, lib: lib}
}

func (o repo) Create(domain *models.DeviceDomain) error {
	ctx := context.TODO()

	obj, err := o.lib.Db.Device().InsertOne(ctx, domain)
	if err != nil {
		return err
	}

	domain.ID = obj.InsertedID.(primitive.ObjectID)
	return nil
}

func (o repo) Update(domain *models.DeviceDomain) error {
	ctx := context.TODO()

	_, err := o.lib.Db.Device().UpdateOne(ctx, models.M{"_id": domain.ID}, models.M{"$set": domain})
	if err != nil {
		return err
	}

	return nil
}

func (o repo) FindAll(req *models.Req) ([]*models.DeviceDomain, error) {
	ctx := context.TODO()
	res := []*models.DeviceDomain{}

	filter := models.M{}
	if len(req.Filters) > 0 {
		filter = models.M{"$and": req.Filters}
	}

	cursor, err := o.lib.Db.Device().Find(ctx, filter, options.Find().SetSkip(req.Skip).SetLimit(req.Limit).SetSort(req.Sort))
	if err != nil {
		return nil, err
	}

	defer cursor.Close(ctx)
	if err := cursor.All(ctx, &res); err != nil {
		return nil, err
	}

	return res, nil
}

func (o repo) Count(req *models.Req) (int64, error) {
	ctx := context.TODO()

	filter := models.M{}
	if len(req.Filters) > 0 {
		filter = models.M{"$and": req.Filters}
	}

	return o.lib.Db.Device().CountDocuments(ctx, filter)
}

func (o repo) FindByDevice(device string) (*models.DeviceDomain, error) {
	ctx := context.TODO()
	res := &models.DeviceDomain{}

	result := o.lib.Db.Device().FindOne(ctx, models.M{"device": device})
	if result.Err() != nil {
		return nil, result.Err()
	}

	result.Decode(&res)
	return res, nil
}

func (o repo) DeleteByToken(token string) error {
	ctx := context.TODO()

	_, err := o.lib.Db.Device().DeleteOne(ctx, models.M{"token": token})
	if err != nil {
		return err
	}

	return nil
}

func (o repo) DeleteByDevice(device string) error {
	ctx := context.TODO()

	_, err := o.lib.Db.Device().DeleteOne(ctx, models.M{"device": device})
	if err != nil {
		return err
	}

	return nil
}
