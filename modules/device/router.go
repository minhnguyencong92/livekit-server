package device

import (
	"app/enums"
	"app/interfaces"
	"app/models"

	"github.com/gofiber/fiber/v2"
)

type ctr struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	srv *interfaces.ISrv
}

func NewRouter(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) *fiber.App {
	c := ctr{rpo: rpo, lib: lib, srv: srv}
	r := fiber.New(fiber.Config{ErrorHandler: lib.Middleware.Error()})

	r.Post("/:device", lib.Oauth.Authenticate(), c.Upsert())

	r.Delete("/:device", c.Delete())

	return r
}

// @Tags Device
// @Summary Upsert
// @Security Bearer
// @Param body body models.DeviceData true "body"
// @Success 200 {object} models.SwagCodeMsg
// @Router /v1/device/{device} [post]
func (o ctr) Upsert() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)

		data := models.DeviceData{}
		if res := o.lib.Middleware.Validate(c, &data); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		res := o.srv.Device.Upsert(&req, c.Params("device"), &data)
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Device
// @Summary Delete
// @Security Bearer
// @Param device path string true "device"
// @Success 200 {object} models.SwagCodeMsg
// @Router /v1/device/{device} [delete]
func (o ctr) Delete() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := o.srv.Device.Delete(&req, c.Params("device"))
		return c.Status(res.Status).JSON(res)
	}
}
