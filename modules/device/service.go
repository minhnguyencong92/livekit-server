package device

import (
	"app/enums"
	"app/interfaces"
	"app/models"
	"time"
)

type service struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	srv *interfaces.ISrv
}

func NewService(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) interfaces.ISrvDevice {
	return service{rpo: rpo, lib: lib, srv: srv}
}

func (o service) Upsert(req *models.Req, device string, data *models.DeviceData) *models.Res {
	domain, err := o.rpo.Device.FindByDevice(device)
	if err != nil {
		domain = &models.DeviceDomain{}
		data.Domain(domain)
		domain.Device = device
		domain.Lang = req.Lang
		domain.User = req.Session.ID
		domain.CreatedAt = time.Now()
		domain.UpdatedAt = time.Now()
		o.rpo.Device.Create(domain)
	} else {
		data.Domain(domain)
		domain.Lang = req.Lang
		domain.User = req.Session.ID
		domain.UpdatedAt = time.Now()
		o.rpo.Device.Update(domain)
	}

	return &models.Res{CodeMsg: enums.Code.Success[req.Lang]}
}

func (o service) Delete(req *models.Req, device string) *models.Res {
	o.rpo.Device.DeleteByDevice(device)
	return &models.Res{CodeMsg: enums.Code.Success[req.Lang]}
}
