package ws

import "app/interfaces"

type service struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	srv *interfaces.ISrv
}

func NewService(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) interfaces.ISrvWs {
	return service{rpo: rpo, lib: lib, srv: srv}
}

func (o service) BroadcastMessageToServer(message any) {
	server.broadcastMessageToServer(message)
}

func (o service) BroadcastMessageToUser(id string, message any) {
	server.broadcastMessageToUser(id, message)
}

func (o service) BroadcastMessageToRoom(id string, message any) {
	server.broadcastMessageToRoom(id, message)
}
