package ws

import (
	"app/interfaces"
	"app/models"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/websocket/v2"
)

func NewRouter(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) *fiber.App {
	r := fiber.New(fiber.Config{ErrorHandler: lib.Middleware.Error()})

	r.Get("/chat", func(c *fiber.Ctx) error {
		return c.Render("chat", models.M{})
	})

	r.Use("/", func(c *fiber.Ctx) error {
		if websocket.IsWebSocketUpgrade(c) {
			return c.Next()
		}
		return fiber.ErrUpgradeRequired
	})

	r.Get("/", websocket.New(func(conn *websocket.Conn) { newClient(conn, rpo, lib, srv) }))

	return r
}
