package ws

import (
	"app/async"
	"app/interfaces"
	"app/models"
	"sync"
)

type wsRoom struct {
	id        string
	rpo       *interfaces.IRpo
	lib       *interfaces.ILib
	srv       *interfaces.ISrv
	clients   [maxClient]*wsClient
	send      chan any
	broadcast chan any
}

func newRoom(id string, rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) *wsRoom {
	room := &wsRoom{
		id:        id,
		rpo:       rpo,
		lib:       lib,
		srv:       srv,
		send:      make(chan any, maxBuffer),
		broadcast: make(chan any, maxBuffer),
	}
	room.run()
	return room
}

func (o *wsRoom) run() {
	async.New().Go(func() { o.loop() })
}

func (o *wsRoom) loop() {
	for {
		select {
		case message := <-o.send:
			o.sendMessage(message)
		case message := <-o.broadcast:
			o.broadcastMessage(message)
		}
	}
}

func (o *wsRoom) register(client *wsClient) {
	o.clients[client.id] = client
}

func (o *wsRoom) unregister(client *wsClient) {
	o.clients[client.id] = nil
}

func (o *wsRoom) broadcastMessage(message any) {
	o.lib.Rdb.Publish(channelRoom, models.WsPubSub{Room: o.id, Message: message})
}

func (o *wsRoom) sendMessage(message any) {
	for _, client := range o.clients {
		if client != nil {
			client.send <- message
		}
	}
}

type wsRooms struct {
	rpo    *interfaces.IRpo
	lib    *interfaces.ILib
	srv    *interfaces.ISrv
	mutex  *sync.Mutex
	arooms []*wsRoom
	mrooms map[string]int
}

func newRooms(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) *wsRooms {
	return &wsRooms{
		rpo:    rpo,
		lib:    lib,
		srv:    srv,
		mutex:  new(sync.Mutex),
		arooms: make([]*wsRoom, 0),
		mrooms: make(map[string]int),
	}
}

func (o *wsRooms) load(id string) *wsRoom {
	if idx, ok := o.mrooms[id]; ok {
		return o.arooms[idx]
	} else {
		return nil
	}
}

func (o *wsRooms) store(id string) *wsRoom {
	if found := o.load(id); found != nil {
		return found
	}

	o.mutex.Lock()
	defer o.mutex.Unlock()

	room := newRoom(id, o.rpo, o.lib, o.srv)
	o.arooms = append(o.arooms, room)
	o.mrooms[id] = len(o.arooms) - 1

	return room
}

func (o *wsRooms) add(room *wsRoom) {
	if found := o.load(room.id); found != nil {
		return
	}

	o.mutex.Lock()
	defer o.mutex.Unlock()

	o.arooms = append(o.arooms, room)
	o.mrooms[room.id] = len(o.arooms) - 1
}

func (o *wsRooms) del(room *wsRoom) {
	o.mutex.Lock()
	defer o.mutex.Unlock()

	lastIndex := len(o.arooms) - 1
	currentIndex := o.mrooms[room.id]

	if currentIndex != lastIndex {
		lastRoom := o.arooms[lastIndex]
		o.arooms[currentIndex] = lastRoom
		o.mrooms[lastRoom.id] = currentIndex
	}

	o.arooms = o.arooms[:lastIndex]
	delete(o.mrooms, room.id)
}

func (o *wsRooms) unregister(client *wsClient) {
	for _, room := range o.arooms {
		room.unregister(client)
	}
}
