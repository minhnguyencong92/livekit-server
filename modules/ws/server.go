package ws

import (
	"app/async"
	"app/interfaces"
	"app/models"
	"encoding/json"
)

const (
	channelServer = "ws_server"
	channelUser   = "ws_user"
	channelRoom   = "ws_room"
	maxClient     = 20_000
	maxBuffer     = 1024
)

var server *wsServer

type wsServer struct {
	rpo       *interfaces.IRpo
	lib       *interfaces.ILib
	srv       *interfaces.ISrv
	rooms     *wsRooms
	clients   [maxClient]*wsClient
	send      chan any
	broadcast chan any
}

func NewServer(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) {
	server = &wsServer{
		rpo:       rpo,
		lib:       lib,
		srv:       srv,
		rooms:     newRooms(rpo, lib, srv),
		send:      make(chan any, maxBuffer),
		broadcast: make(chan any, maxBuffer),
	}
	server.run()
}

func (o *wsServer) run() {
	as := async.New()
	as.Go(func() { o.loop() })
	as.Go(func() { o.subscribeMessage() })
}

func (o *wsServer) loop() {
	for {
		select {
		case message := <-o.send:
			o.sendMessageToServer(message)
		case message := <-o.broadcast:
			o.broadcastMessageToServer(message)
		}
	}
}

func (o *wsServer) register(client *wsClient) {
	o.clients[client.id] = client
}

func (o *wsServer) unregister(client *wsClient) {
	o.clients[client.id] = nil
}

func (o *wsServer) broadcastMessageToServer(message any) {
	o.lib.Rdb.Publish(channelServer, models.WsPubSub{Message: message})
}

func (o *wsServer) broadcastMessageToUser(id string, message any) {
	o.lib.Rdb.Publish(channelUser, models.WsPubSub{User: id, Message: message})
}

func (o *wsServer) broadcastMessageToRoom(id string, message any) {
	o.lib.Rdb.Publish(channelRoom, models.WsPubSub{Room: id, Message: message})
}

func (o *wsServer) subscribeMessage() {
	ch := o.lib.Rdb.Subscribe(channelServer, channelUser, channelRoom).Channel()
	for msg := range ch {
		var payload models.WsPubSub
		json.Unmarshal([]byte(msg.Payload), &payload)

		switch msg.Channel {
		case channelServer:
			o.sendMessageToServer(payload.Message)
		case channelUser:
			o.sendMessageToUser(payload.User, payload.Message)
		case channelRoom:
			o.sendMessageToRoom(payload.Room, payload.Message)
		}
	}
}

func (o *wsServer) sendMessageToServer(message any) {
	for _, client := range o.clients {
		if client != nil {
			client.send <- message
		}
	}
}

func (o *wsServer) sendMessageToUser(id string, message any) {
	for _, client := range o.clients {
		if client != nil && client.req.Session.ID == id {
			client.send <- message
		}
	}
}

func (o *wsServer) sendMessageToRoom(id string, message any) {
	if room := o.rooms.load(id); room != nil {
		room.send <- message
	}
}
