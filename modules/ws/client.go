package ws

import (
	"app/async"
	"app/clog"
	"app/enums"
	"app/interfaces"
	"app/models"
	"reflect"
	"time"

	fws "github.com/fasthttp/websocket"
	"github.com/gofiber/websocket/v2"
)

type wsClient struct {
	id        int
	rpo       *interfaces.IRpo
	lib       *interfaces.ILib
	srv       *interfaces.ISrv
	req       *models.Req
	conn      *websocket.Conn
	rooms     *wsRooms
	send      chan any
	broadcast chan any
}

func newClient(conn *websocket.Conn, rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) {
	req := conn.Locals(enums.Constant.Req).(models.Req)
	conn.WriteJSON(models.Res{CodeMsg: enums.Code.SocketConnected[req.Lang]})

	client := &wsClient{
		id:        getSysfd(conn),
		rpo:       rpo,
		lib:       lib,
		srv:       srv,
		req:       &req,
		conn:      conn,
		rooms:     newRooms(rpo, lib, srv),
		send:      make(chan any, maxBuffer),
		broadcast: make(chan any, maxBuffer),
	}

	defer client.close()
	client.run()
}

func getSysfd(conn *websocket.Conn) int {
	connNet := conn.Conn.UnderlyingConn()
	connInterface := reflect.Indirect(reflect.ValueOf(connNet)).FieldByName("Conn")

	connPointer := reflect.Indirect(reflect.New(connInterface.Elem().Type()))
	connPointer.Set(connInterface.Elem())

	connVal := reflect.Indirect(connPointer).FieldByName("conn")
	fdPointer := connVal.FieldByName("fd")

	pfdVal := reflect.Indirect(fdPointer).FieldByName("pfd")
	sysfd := pfdVal.FieldByName("Sysfd").Int()

	return int(sysfd)
}

func (o *wsClient) close() {
	if r := recover(); r != nil {
		if _, ok := r.(*fws.CloseError); !ok {
			clog.Errorc(&models.CLog{Struct: wsClient{}, Error: r})
		}
		clog.Info(r)
		server.unregister(o)
		o.rooms.unregister(o)
		o.conn.Close()
	}
}

func (o *wsClient) run() {
	async.New().Go(func() { o.loop() })
	server.register(o)
	o.receiveMessage()
}

func (o *wsClient) loop() {
	for {
		select {
		case message := <-o.send:
			o.sendMessage(message)
		case message := <-o.broadcast:
			o.broadcastMessage(message)
		}
	}
}

func (o *wsClient) broadcastMessage(message any) {
	o.lib.Rdb.Publish(channelUser, models.WsPubSub{User: o.req.Session.ID, Message: message})
}

func (o *wsClient) sendMessage(message any) {
	o.conn.WriteJSON(models.Res{CodeMsg: enums.Code.Success[o.req.Lang], Data: message})
}

func (o *wsClient) receiveMessage() {
	for {
		var message models.WsMessageData
		if err := o.conn.ReadJSON(&message); err != nil {
			o.handleError(err)
		} else if err := o.lib.Util.Validate(message); err != nil {
			o.conn.WriteJSON(models.Res{CodeMsg: enums.Code.BadRequest[o.req.Lang], Error: err.Error()})
		} else {
			o.handleNewMessage(&message)
		}
	}
}

func (o *wsClient) handleError(err error) {
	codes := []int{
		websocket.CloseNormalClosure,
		websocket.CloseGoingAway,
		websocket.CloseProtocolError,
		websocket.CloseUnsupportedData,
		websocket.CloseNoStatusReceived,
		websocket.CloseAbnormalClosure,
		websocket.CloseInvalidFramePayloadData,
		websocket.ClosePolicyViolation,
		websocket.CloseMessageTooBig,
		websocket.CloseMandatoryExtension,
		websocket.CloseInternalServerErr,
		websocket.CloseServiceRestart,
		websocket.CloseTryAgainLater,
		websocket.CloseTLSHandshake,
	}
	if websocket.IsCloseError(err, codes...) {
		panic(err)
	}
	o.conn.WriteJSON(models.Res{CodeMsg: enums.Code.Error[o.req.Lang], Error: err.Error()})
}

func (o *wsClient) handleNewMessage(message *models.WsMessageData) {
	switch message.Action {
	case "call":
		
	case "join-room":
		data := models.WsJoinRoomData{}
		o.lib.Util.Convert(message, &data)
		o.handleJoinRoom(&data)
	case "leave-room":
		data := models.WsLeaveRoomData{}
		o.lib.Util.Convert(message, &data)
		o.handleLeaveRoom(&data)
	case "send-message":
		data := models.WsSendMessageData{}
		o.lib.Util.Convert(message, &data)
		o.handleSendMessage(&data)
	default:
		o.conn.WriteJSON(models.Res{CodeMsg: enums.Code.BadRequest[o.req.Lang], Data: "No action"})
	}
}

// @Tags Websocket
// @Summary WsJoinRoom
// @Security Bearer
// @Param body body models.WsJoinRoomData true "body"
// @Success 200 {object} models.SwagWsJoinRoomDto
// @Router /join-room [post]
func (o *wsClient) handleJoinRoom(data *models.WsJoinRoomData) {
	// TODO: check client in room
	room := server.rooms.store(data.Payload.Room)
	room.register(o)
	o.rooms.add(room)
	o.sendMessage(&models.WsJoinRoomDto{Action: data.Action, Payload: data.Payload, CreatedAt: time.Now(), Sender: o.req.Session.BaseDto()})
}

// @Tags Websocket
// @Summary WsLeaveRoom
// @Security Bearer
// @Param body body models.WsLeaveRoomData true "body"
// @Success 200 {object} models.SwagWsLeaveRoomDto
// @Router /leave-room [post]
func (o *wsClient) handleLeaveRoom(data *models.WsLeaveRoomData) {
	if room := o.rooms.load(data.Payload.Room); room != nil {
		room.unregister(o)
		o.rooms.del(room)
		o.sendMessage(&models.WsLeaveRoomDto{Action: data.Action, Payload: data.Payload, CreatedAt: time.Now(), Sender: o.req.Session.BaseDto()})
	}
}

// @Tags Websocket
// @Summary WsSendMessage
// @Security Bearer
// @Param body body models.WsSendMessageData true "body"
// @Success 200 {object} models.SwagWsSendMessageDto
// @Router /send-message [post]
func (o *wsClient) handleSendMessage(data *models.WsSendMessageData) {
	if room := o.rooms.load(data.Payload.Room); room != nil {
		room.broadcast <- &models.WsSendMessageDto{Action: data.Action, Payload: data.Payload, CreatedAt: time.Now(), Sender: o.req.Session.BaseDto()}
	}
}
