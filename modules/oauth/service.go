package oauth

import (
	"app/enums"
	"app/interfaces"
	"app/models"
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"
)

type service struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	srv *interfaces.ISrv
}

func NewService(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) interfaces.ISrvOauth {
	return service{rpo: rpo, lib: lib, srv: srv}
}

func (o service) Login(req *models.Req, body *models.OauthLoginBody) *models.Res {
	loginPayload, _ := json.Marshal(models.OauthClientBody{
		Username:  body.Username,
		Password:  body.Password,
		GrantType: "password",
	})

	loginHttp, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("http://localhost:%v/v1/oauth/token", enums.Env.Port), bytes.NewBuffer(loginPayload))
	authorization := base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%v:%v", enums.Env.ClientId, enums.Env.ClientSecret)))
	loginHttp.Header.Set("Authorization", fmt.Sprintf("Basic %v", authorization))
	loginHttp.Header.Set("Content-Type", "application/json")

	loginClient, err := new(http.Client).Do(loginHttp)
	if err != nil {
		return &models.Res{CodeMsg: enums.Code.Error[req.Lang], Error: err.Error()}
	}

	loginRes := &models.Res{}
	json.NewDecoder(loginClient.Body).Decode(&loginRes)

	loginClient.Body.Close()
	return loginRes
}

func (o service) Register(req *models.Req, body *models.OauthRegisterBody) *models.Res {
	user, err := o.rpo.User.FindByUsername(body.Username)
	if err != nil {
		user = &models.UserDomain{}
		user.Status = enums.Meta.StatusUser.Activated
		user.CreatedAt = time.Now()
	} else if user.Status != enums.Meta.StatusUser.Waiting {
		return &models.Res{CodeMsg: enums.Code.UserConflict[req.Lang]}
	}

	otp := o.lib.Util.RandomString(&models.RandomString{IsNumber: true})
	if strings.HasPrefix(body.Username, enums.Constant.OtpPrefix) {
		otp = enums.Constant.OtpDefault
	}

	user.OtpCode = otp
	user.Name = body.Name
	user.Username = body.Username
	user.OtpExpires = time.Now().Add(time.Duration(enums.Constant.OtpExpires) * time.Minute)
	user.Secret = o.lib.Util.GeneratorSecret()
	user.Password = o.lib.Util.Encrypt(body.Password, user.Secret)
	user.UpdatedAt = time.Now()
	o.rpo.User.UpsertByUsername(user)

	return &models.Res{CodeMsg: enums.Code.Success[req.Lang]}
}

func (o service) RegisterOtp(req *models.Req, body *models.OauthRegisterOtpBody) *models.Res {
	user, err := o.rpo.User.FindByUsername(body.Username)
	if err != nil {
		return &models.Res{CodeMsg: enums.Code.UserNotFound[req.Lang]}
	}

	if user.OtpCode != body.Code {
		return &models.Res{CodeMsg: enums.Code.CodeInvalid[req.Lang]}
	}

	if user.OtpExpires.Before(time.Now()) {
		return &models.Res{CodeMsg: enums.Code.CodeExpired[req.Lang]}
	}

	user.OtpCode = ""
	user.Status = enums.Meta.StatusUser.Activated
	o.rpo.User.Update(user)

	return &models.Res{CodeMsg: enums.Code.Success[req.Lang]}
}

func (o service) ForgotPassword(req *models.Req, body *models.OauthForgotPasswordBody) *models.Res {
	user, err := o.rpo.User.FindByUsername(body.Username)
	if err != nil {
		return &models.Res{CodeMsg: enums.Code.UserNotFound[req.Lang]}
	}

	otp := o.lib.Util.RandomString(&models.RandomString{IsNumber: true})
	if strings.HasPrefix(body.Username, enums.Constant.OtpPrefix) {
		otp = enums.Constant.OtpDefault
	}

	user.OtpCode = otp
	user.OtpExpires = time.Now().Add(time.Duration(enums.Constant.OtpExpires) * time.Minute)
	o.rpo.User.Update(user)

	return &models.Res{CodeMsg: enums.Code.Success[req.Lang]}
}

func (o service) ForgotPasswordOtp(req *models.Req, body *models.OauthForgotPasswordOtpBody) *models.Res {
	user, err := o.rpo.User.FindByUsername(body.Username)
	if err != nil {
		return &models.Res{CodeMsg: enums.Code.UserNotFound[req.Lang]}
	}

	if user.OtpCode != body.Code {
		return &models.Res{CodeMsg: enums.Code.CodeInvalid[req.Lang]}
	}

	if user.OtpExpires.Before(time.Now()) {
		return &models.Res{CodeMsg: enums.Code.CodeExpired[req.Lang]}
	}

	user.OtpCode = ""
	user.Secret = o.lib.Util.GeneratorSecret()
	user.Password = o.lib.Util.Encrypt(body.Password, user.Secret)
	o.rpo.User.Update(user)
	o.rpo.Token.Delete(&models.TokenData{UserId: user.ID.Hex(), IsDeleteDB: true})

	return &models.Res{CodeMsg: enums.Code.Success[req.Lang]}
}

func (o service) ChangePassword(req *models.Req, body *models.OauthChangePasswordBody) *models.Res {
	user, err := o.rpo.User.FindByUsername(body.Username)
	if err != nil {
		return &models.Res{CodeMsg: enums.Code.UserNotFound[req.Lang]}
	}

	if o.lib.Util.Decrypt(user.Password, user.Secret) != body.OldPassword {
		return &models.Res{CodeMsg: enums.Code.OldPasswordInvalid[req.Lang]}
	}

	user.Secret = o.lib.Util.GeneratorSecret()
	user.Password = o.lib.Util.Encrypt(body.NewPassword, user.Secret)
	o.rpo.User.Update(user)
	o.rpo.Token.Delete(&models.TokenData{UserId: user.ID.Hex(), IsDeleteDB: true})

	return &models.Res{CodeMsg: enums.Code.Success[req.Lang]}
}

func (o service) DeleteMe(req *models.Req, body *models.OauthDeleteBody) *models.Res {
	if body.AccessToken != "" {
		o.rpo.Token.Delete(&models.TokenData{AccessToken: body.AccessToken, IsDeleteDB: true})
	} else {
		o.rpo.Token.Delete(&models.TokenData{UserId: body.UserId, IsDeleteDB: true})
	}

	return &models.Res{CodeMsg: enums.Code.Success[req.Lang]}
}

func (o service) UpdateMe(req *models.Req, data *models.UserMeData) *models.Res {
	domain, err := o.rpo.User.FindById(data.ID)
	if err != nil {
		return &models.Res{CodeMsg: enums.Code.UserNotFound[req.Lang]}
	}

	data.Domain(domain)
	domain.UpdatedAt = time.Now()
	o.rpo.User.Update(domain)

	o.rpo.Token.Delete(&models.TokenData{UserId: domain.ID.Hex()})

	return &models.Res{CodeMsg: enums.Code.Success[req.Lang]}
}
