package oauth

import (
	"app/enums"
	"app/interfaces"
	"app/models"
	"strings"

	"github.com/gofiber/fiber/v2"
)

type ctr struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	srv *interfaces.ISrv
}

func NewRouter(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) *fiber.App {
	c := ctr{rpo: rpo, lib: lib, srv: srv}
	r := fiber.New(fiber.Config{ErrorHandler: lib.Middleware.Error()})

	r.Post("/token", lib.Oauth.Token())

	r.Post("/login", c.Login())

	r.Post("/register", c.Register())

	r.Post("/register/otp", c.RegisterOtp())

	r.Post("/forgotpass", c.ForgotPassword())

	r.Post("/forgotpass/otp", c.ForgotPasswordOtp())

	r.Put("/changepass", lib.Oauth.Authenticate(), c.ChangePassword())

	r.Put("/me", lib.Oauth.Authenticate(), c.UpdateMe())

	r.Get("/me", lib.Oauth.Authenticate(), c.GetMe())

	r.Delete("/me/:user", c.DeleteMe())

	r.Delete("/cache/:key?", lib.Oauth.Authenticate(), lib.Oauth.Authorize(enums.Permission.Root), c.DeleteCache())

	r.Get("/cache/:key?", lib.Oauth.Authenticate(), lib.Oauth.Authorize(enums.Permission.Root), c.GetCache())

	return r
}

// @Tags Oauth
// @Summary Login
// @Param body body models.OauthLoginBody true "body"
// @Success 200 {object} models.SwagOauthTokenBody
// @Router /v1/oauth/login [post]
func (o ctr) Login() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)

		body := models.OauthLoginBody{}
		if res := o.lib.Middleware.Validate(c, &body); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		res := o.srv.Oauth.Login(&req, &body)
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Oauth
// @Summary Register
// @Param body body models.OauthRegisterBody true "body"
// @Success 200 {object} models.SwagCodeMsg
// @Router /v1/oauth/register [post]
func (o ctr) Register() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)

		body := models.OauthRegisterBody{}
		if res := o.lib.Middleware.Validate(c, &body); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		res := o.srv.Oauth.Register(&req, &body)
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Oauth
// @Summary RegisterOtp
// @Param body body models.OauthRegisterOtpBody true "body"
// @Success 200 {object} models.SwagCodeMsg
// @Router /v1/oauth/register/otp [post]
func (o ctr) RegisterOtp() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)

		body := models.OauthRegisterOtpBody{}
		if res := o.lib.Middleware.Validate(c, &body); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		res := o.srv.Oauth.RegisterOtp(&req, &body)
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Oauth
// @Summary ForgotPassword
// @Param body body models.OauthForgotPasswordBody true "body"
// @Success 200 {object} models.SwagCodeMsg
// @Router /v1/oauth/forgotpass [post]
func (o ctr) ForgotPassword() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)

		body := models.OauthForgotPasswordBody{}
		if res := o.lib.Middleware.Validate(c, &body); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		res := o.srv.Oauth.ForgotPassword(&req, &body)
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Oauth
// @Summary ForgotPasswordOtp
// @Param body body models.OauthForgotPasswordOtpBody true "body"
// @Success 200 {object} models.SwagCodeMsg
// @Router /v1/oauth/forgotpass/otp [post]
func (o ctr) ForgotPasswordOtp() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)

		body := models.OauthForgotPasswordOtpBody{}
		if res := o.lib.Middleware.Validate(c, &body); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		res := o.srv.Oauth.ForgotPasswordOtp(&req, &body)
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Oauth
// @Summary ChangePassword
// @Security Bearer
// @Param body body models.OauthChangePasswordBody true "body"
// @Success 200 {object} models.SwagCodeMsg
// @Router /v1/oauth/changepass [post]
func (o ctr) ChangePassword() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)

		body := models.OauthChangePasswordBody{}
		if res := o.lib.Middleware.Validate(c, &body); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		body.Username = req.Session.Username

		res := o.srv.Oauth.ChangePassword(&req, &body)
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Oauth
// @Summary UpdateMe
// @Security Bearer
// @Param body body models.UserMeData true "body"
// @Success 200 {object} models.SwagCodeMsg
// @Router /v1/oauth/me [put]
func (o ctr) UpdateMe() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)

		data := models.UserMeData{}
		if res := o.lib.Middleware.Validate(c, &data); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		data.ID = req.Session.ID

		res := o.srv.Oauth.UpdateMe(&req, &data)
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Oauth
// @Summary DeleteMe
// @Param user path string true "user"
// @Success 200 {object} models.SwagCodeMsg
// @Router /v1/oauth/me/{user} [delete]
func (o ctr) DeleteMe() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)

		body := models.OauthDeleteBody{}
		body.UserId = c.Params("user")
		body.AccessToken = c.Query("authorization")

		res := o.srv.Oauth.DeleteMe(&req, &body)
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Oauth
// @Summary GetMe
// @Security Bearer
// @Success 200 {object} models.SwagOauthSessionDto
// @Router /v1/oauth/me [get]
func (o ctr) GetMe() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := models.Res{CodeMsg: enums.Code.Success[req.Lang], Data: req.Session}
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Oauth
// @Summary DeleteCache
// @Security Bearer
// @Param key query string false "key"
// @Success 200 {object} models.SwagCodeMsg
// @Router /v1/oauth/cache [delete]
func (o ctr) DeleteCache() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := models.Res{CodeMsg: enums.Code.Success[req.Lang]}
		o.lib.Rdb.Dels(c.Query("key"))
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Oauth
// @Summary GetCache
// @Security Bearer
// @Param key query string false "key (add prefix detail- to view detail)"
// @Success 200 {object} models.SwagCodeMsg
// @Router /v1/oauth/cache [get]
func (o ctr) GetCache() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := models.Res{CodeMsg: enums.Code.Success[req.Lang]}
		if strings.HasPrefix(c.Query("key"), "detail-") {
			var data any
			if err := o.lib.Rdb.Get(strings.Split(c.Query("key"), "detail-")[1], &data); err != nil {
				res.Error = err.Error()
			}
			res.Data = data
		} else {
			res.Data = o.lib.Rdb.Keys(c.Query("key"))
		}
		return c.Status(res.Status).JSON(res)
	}
}
