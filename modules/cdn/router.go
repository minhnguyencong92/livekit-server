package cdn

import (
	"app/async"
	"app/clog"
	"app/enums"
	"app/interfaces"
	"app/models"
	"fmt"
	"path"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

type ctr struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	srv *interfaces.ISrv
}

func NewRouter(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) *fiber.App {
	c := ctr{rpo: rpo, lib: lib, srv: srv}
	r := fiber.New(fiber.Config{ErrorHandler: lib.Middleware.Error()})

	r.Post("/:kind", lib.Oauth.Authenticate(), c.Upload())

	r.Get("/:kind/:name", c.Download())

	return r
}

// @Tags Cdn
// @Summary Upload
// @Security Bearer
// @Param kind path string true "kind" Enums(image, video)
// @Param files formData file true "files"
// @Success 200 {object} models.SwagCodeMsg
// @Router /v1/cdn/{kind} [post]
func (o ctr) Upload() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		kind := c.Params("kind")

		size, ok := enums.Constant.FileValid[kind]
		if !ok {
			res := models.Res{CodeMsg: enums.Code.ApiNotFound[req.Lang]}
			return c.Status(res.Status).JSON(res)
		}

		form, err := c.MultipartForm()
		if err != nil {
			res := models.Res{CodeMsg: enums.Code.Error[req.Lang], Error: err.Error()}
			return c.Status(res.Status).JSON(res)
		}

		files := form.File["files"]

		if len(files) == 0 {
			res := models.Res{CodeMsg: enums.Code.FileNotFound[req.Lang]}
			return c.Status(res.Status).JSON(res)
		}

		for _, file := range files {
			if file.Size > size.(int64) {
				res := models.Res{CodeMsg: enums.Code.FileTooLarge[req.Lang]}
				return c.Status(res.Status).JSON(res)
			} else if o.lib.Util.Includes([]string{"image", "video"}, kind) &&
				!strings.HasPrefix(file.Header.Values("Content-Type")[0], kind) {
				res := models.Res{CodeMsg: enums.Code.FileUnsupport[req.Lang]}
				return c.Status(res.Status).JSON(res)
			}
		}

		as := async.New()
		data := make([]string, len(files))

		for idx, file := range files {
			idx := idx
			file := file
			as.Go(func() {
				if stream, err := file.Open(); err == nil {
					data[idx] = fmt.Sprintf("/%v/%v%v", kind, uuid.NewString(), path.Ext(file.Filename))
					if _, err := o.lib.Storage.Upload(data[idx], stream, file.Size, file.Header.Values("Content-Type")[0]); err != nil {
						clog.Errorc(&models.CLog{Struct: ctr{}, Func: "Upload()", Error: err.Error()})
					}
					// o.lib.Db.Bucket().UploadFromStream(data[idx], stream, options.GridFSUpload().SetMetadata(models.M{
					// 	"contentType": file.Header.Values("Content-Type")[0],
					// }))
				}
			})
		}

		if err := as.Wait(); err != nil {
			res := models.Res{CodeMsg: enums.Code.Error[req.Lang], Error: err.Error()}
			return c.Status(res.Status).JSON(res)
		}

		res := models.Res{CodeMsg: enums.Code.Success[req.Lang], Data: data}
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Cdn
// @Summary Download
// @Param kind path string true "kind" Enums(image, video)
// @Param name path string true "name"
// @Router /v1/cdn/{kind}/{name} [get]
func (o ctr) Download() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		kind := c.Params("kind")
		name := c.Params("name")
		filename := fmt.Sprintf("/%v/%v", kind, name)

		if _, ok := enums.Constant.FileValid[kind]; !ok {
			res := models.Res{CodeMsg: enums.Code.ApiNotFound[req.Lang]}
			return c.Status(res.Status).JSON(res)
		}

		c.Response().Header.SetContentType(fmt.Sprintf("%v/%v", kind, strings.Replace(path.Ext(name), ".", "", 1)))

		// if _, err := o.lib.Db.Bucket().DownloadToStreamByName(filename, c.Response().BodyWriter()); err != nil {
		// 	res := models.Res{CodeMsg: enums.Code.FileNotFound[req.Lang]}
		// 	return c.Status(res.Status).JSON(res)
		// }

		if file, err := o.lib.Storage.Download(filename); err != nil {
			res := models.Res{CodeMsg: enums.Code.FileNotFound[req.Lang]}
			return c.Status(res.Status).JSON(res)
		} else {
			c.SendStream(file)
		}

		return nil
	}
}
