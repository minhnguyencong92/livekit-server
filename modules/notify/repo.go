package notify

import (
	"app/interfaces"
	"app/models"
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type repo struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
}

func NewRepo(rpo *interfaces.IRpo, lib *interfaces.ILib) interfaces.IRpoNotify {
	return repo{rpo: rpo, lib: lib}
}

func (o repo) Create(domain *models.NotifyDomain) error {
	ctx := context.TODO()

	obj, err := o.lib.Db.Notify().InsertOne(ctx, domain)
	if err != nil {
		return err
	}

	domain.ID = obj.InsertedID.(primitive.ObjectID)
	return nil
}

func (o repo) FindAll(req *models.Req) ([]*models.NotifyDomain, error) {
	ctx := context.TODO()
	res := []*models.NotifyDomain{}

	filter := models.M{}
	if len(req.Filters) > 0 {
		filter = models.M{"$and": req.Filters}
	}

	cursor, err := o.lib.Db.Notify().Find(ctx, filter, options.Find().SetSkip(req.Skip).SetLimit(req.Limit).SetSort(req.Sort))
	if err != nil {
		return nil, err
	}

	defer cursor.Close(ctx)
	if err := cursor.All(ctx, &res); err != nil {
		return nil, err
	}

	return res, nil
}

func (o repo) Count(req *models.Req) (int64, error) {
	ctx := context.TODO()

	filter := models.M{}
	if len(req.Filters) > 0 {
		filter = models.M{"$and": req.Filters}
	}

	return o.lib.Db.Notify().CountDocuments(ctx, filter)
}

func (o repo) Read(id, receiver string) error {
	ctx := context.TODO()
	oid, _ := primitive.ObjectIDFromHex(id)

	if oid.IsZero() {
		_, err := o.lib.Db.Notify().UpdateMany(ctx, models.M{"unreaders": receiver}, models.M{"$pull": models.M{"unreaders": receiver}})
		return err
	} else {
		_, err := o.lib.Db.Notify().UpdateOne(ctx, models.M{"_id": oid}, models.M{"$pull": models.M{"unreaders": receiver}})
		return err
	}
}

func (o repo) Delete(id, receiver string) error {
	ctx := context.TODO()
	oid, _ := primitive.ObjectIDFromHex(id)

	if oid.IsZero() {
		_, err := o.lib.Db.Notify().UpdateMany(ctx, models.M{"receivers": receiver}, models.M{"$pull": models.M{"receivers": receiver}})
		return err
	} else {
		_, err := o.lib.Db.Notify().UpdateOne(ctx, models.M{"_id": oid}, models.M{"$pull": models.M{"receivers": receiver}})
		return err
	}
}
