package notify

import (
	"app/async"
	"app/enums"
	"app/interfaces"
	"app/models"
	"strings"
)

type service struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	srv *interfaces.ISrv
}

func NewService(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) interfaces.ISrvNotify {
	return service{rpo: rpo, lib: lib, srv: srv}
}

func (o service) Read(req *models.Req, id string) *models.Res {
	o.rpo.Notify.Read(id, req.Session.ID)
	return &models.Res{CodeMsg: enums.Code.Success[req.Lang]}
}

func (o service) Delete(req *models.Req, id string) *models.Res {
	o.rpo.Notify.Delete(id, req.Session.ID)
	return &models.Res{CodeMsg: enums.Code.Success[req.Lang]}
}

func (o service) FindAll(req *models.Req) *models.Res {
	domains, err := o.rpo.Notify.FindAll(req)
	if err != nil {
		return &models.Res{CodeMsg: enums.Code.Error[req.Lang], Error: err.Error()}
	}

	list := make([]models.NotifyDto, len(domains))

	as := async.New()
	for idx, domain := range domains {
		idx := idx
		domain := domain
		as.Go(func() {
			dto := models.NotifyDto{
				ID:          domain.ID.Hex(),
				Title:       domain.Title,
				Content:     domain.Content,
				Description: domain.Description,
				IsRead:      !o.lib.Util.Includes(domain.Unreaders, req.Session.ID),
				Opts:        domain.Opts,
				Group:       enums.Meta.Get(domain.Group, req.Lang),
				CreatedAt:   domain.CreatedAt,
				UpdatedAt:   domain.UpdatedAt,
			}
			if domain.Kind != "" {
				dto.Kind = enums.Meta.Get(domain.Kind, req.Lang)
				dto.Title = strings.Replace(dto.Kind.Name, "{name}", domain.Opts.Name, 1)
			}
			if sender, err := o.rpo.User.FindById(domain.Sender); err == nil {
				dto.Sender = sender.BaseDto()
			}
			list[idx] = dto
		})
	}
	if err := as.Wait(); err != nil {
		return &models.Res{CodeMsg: enums.Code.Error[req.Lang], Error: err.Error()}
	}

	req.Filters = append(req.Filters, models.M{"unreaders": req.Session.ID})
	total, _ := o.rpo.Notify.Count(req)

	return &models.Res{CodeMsg: enums.Code.Success[req.Lang], Data: list, Total: total}
}
