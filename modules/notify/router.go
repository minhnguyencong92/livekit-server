package notify

import (
	"app/enums"
	"app/interfaces"
	"app/models"

	"github.com/gofiber/fiber/v2"
)

type ctr struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	srv *interfaces.ISrv
}

func NewRouter(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) *fiber.App {
	c := ctr{rpo: rpo, lib: lib, srv: srv}
	r := fiber.New(fiber.Config{ErrorHandler: lib.Middleware.Error()})

	r.Put("/:id", lib.Oauth.Authenticate(), c.Read())

	r.Delete("/:id", lib.Oauth.Authenticate(), c.Delete())

	r.Get("/", lib.Oauth.Authenticate(), c.FindAll())

	return r
}

// @Tags Notify
// @Summary Read
// @Security Bearer
// @Param id path string true "id"
// @Success 200 {object} models.SwagCodeMsg
// @Router /v1/notify/{id} [put]
func (o ctr) Read() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := o.srv.Notify.Read(&req, c.Params("id"))
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Notify
// @Summary Delete
// @Security Bearer
// @Param id path string true "id"
// @Success 200 {object} models.SwagCodeMsg
// @Router /v1/notify/{id} [delete]
func (o ctr) Delete() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := o.srv.Notify.Delete(&req, c.Params("id"))
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Notify
// @Summary FindAll
// @Security Bearer
// @Param page query integer false "page" default(1)
// @Param limit query integer false "limit" default(1)
// @Param sorts query string false "sorts" default(createdAt:-1)
// @Param group_eq query string false "group_eq"
// @Success 200 {object} models.SwagNotifyDtoList
// @Router /v1/notify [get]
func (o ctr) FindAll() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)

		data := models.NotifyFilter{}
		if res := o.lib.Middleware.Validate(c, &data); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		req.Filters = append(req.Filters, models.M{"receivers": req.Session.ID})

		if data.Group_Eq != "" {
			req.Filters = append(req.Filters, models.M{"group": data.Group_Eq})
		}

		res := o.srv.Notify.FindAll(&req)
		return c.Status(res.Status).JSON(res)
	}
}
