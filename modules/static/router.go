package static

import (
	"app/enums"
	"app/interfaces"
	"app/models"

	"github.com/gofiber/fiber/v2"
)

type ctr struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	srv *interfaces.ISrv
}

func NewRouter(rpo *interfaces.IRpo, lib *interfaces.ILib, srv *interfaces.ISrv) *fiber.App {
	c := ctr{rpo: rpo, lib: lib, srv: srv}
	r := fiber.New(fiber.Config{ErrorHandler: lib.Middleware.Error()})

	r.Get("/config", c.Config())

	r.Get("/province", c.Province())

	r.Get("/district", c.District())

	r.Get("/ward", c.Ward())

	r.Get("/meta", c.Meta())

	return r
}

// @Tags Static
// @Summary Config
// @Success 200 {object} models.SwagConfigDto
// @Router /v1/static/config [get]
func (o ctr) Config() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := models.Res{CodeMsg: enums.Code.Success[req.Lang]}
		res.Data = models.ConfigDto{
			CdnUrl: enums.Env.CdnUrl,
		}
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Static
// @Summary Province
// @Param isCity_eq query boolean false "iscity_eq"
// @Success 200 {object} models.SwagProvinceDtoList
// @Router /v1/static/province [get]
func (o ctr) Province() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := models.Res{CodeMsg: enums.Code.Success[req.Lang]}

		data := models.ProvinceFilter{}
		if res := o.lib.Middleware.Validate(c, &data); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		res.Data = enums.Province.List(&data)
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Static
// @Summary District
// @Param province_eq query string true "province_eq"
// @Success 200 {object} models.SwagDistrictDtoList
// @Router /v1/static/district [get]
func (o ctr) District() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := models.Res{CodeMsg: enums.Code.Success[req.Lang]}

		data := models.DistrictFilter{}
		if res := o.lib.Middleware.Validate(c, &data); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		res.Data = enums.District.List(&data)
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Static
// @Summary Ward
// @Param district_eq query string true "district_eq"
// @Success 200 {object} models.SwagWardDtoList
// @Router /v1/static/ward [get]
func (o ctr) Ward() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := models.Res{CodeMsg: enums.Code.Success[req.Lang]}

		data := models.WardFilter{}
		if res := o.lib.Middleware.Validate(c, &data); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		res.Data = enums.Ward.List(&data)
		return c.Status(res.Status).JSON(res)
	}
}

// @Tags Static
// @Summary Meta
// @Success 200 {object} models.SwagMetaDto
// @Router /v1/static/meta [get]
func (o ctr) Meta() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := models.Res{CodeMsg: enums.Code.Success[req.Lang]}
		res.Data = enums.Meta.List(req.Lang)
		return c.Status(res.Status).JSON(res)
	}
}
