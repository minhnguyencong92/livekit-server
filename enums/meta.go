package enums

import (
	"app/async"
	"app/models"
	"encoding/json"
	"io"
	"os"
	"sort"
)

type meta struct {
	list          []models.MetaDomain
	obj           map[string]models.MetaDomain
	Notify        notify
	NotifyGroup   notifyGroup
	StatusUser    statusUser
	StatusAccount statusAccount
	StatusCall    statusCall
}

func (o meta) List(lang string) map[string][]*models.MetaDto {
	res := map[string][]*models.MetaDto{}
	for _, v := range o.list {
		dto := models.MetaDto{ID: v.ID, Name: v.Name[lang].(string), Opts: v.Opts}
		if list, ok := res[v.Kind]; ok {
			res[v.Kind] = append(list, &dto)
		} else {
			res[v.Kind] = []*models.MetaDto{&dto}
		}
	}
	return res
}

func (o meta) Get(id, lang string) models.BaseMetaDto {
	if val, ok := o.obj[id]; ok {
		return val.BaseDto(lang)
	}
	return models.BaseMetaDto{}
}

type notify struct{ System string }
type notifyGroup struct{ System string }
type statusAccount struct{ Activated, Locked string }
type statusUser struct{ Waiting, Activated, Locked string }
type statusCall struct{ Success, Busy, Missing string }

var Meta = meta{
	Notify: notify{
		System: "5f2a2522231afd03a5061995",
	},
	NotifyGroup: notifyGroup{
		System: "5f2a256f231afd03a5061996",
	},
	StatusUser: statusUser{
		Waiting:   "5ed349d2e6278057e83adf00",
		Activated: "5ed349d2e6278057e83adf0e",
		Locked:    "5ed349d2e6278057e83adf18",
	},
	StatusAccount: statusAccount{
		Activated: "5ed349fde6278057e83adfde",
		Locked:    "5ed349fde6278057e83adfd0",
	},
	StatusCall: statusCall{
		Success: "success",
		Busy:    "busy",
		Missing: "missing",
	},
}

func init() {
	async.New().Go(func() {
		jsonList, _ := os.Open("json/meta-list.json")
		defer jsonList.Close()
		byteList, _ := io.ReadAll(jsonList)
		json.Unmarshal(byteList, &Meta.list)
		sort.Slice(Meta.list, func(i, j int) bool { return Meta.list[i].Priority-Meta.list[j].Priority < 0 })
		Meta.obj = make(map[string]models.MetaDomain)
		for _, v := range Meta.list {
			Meta.obj[v.ID] = v
		}
	})
}
