package enums

import (
	"app/async"
	"app/models"
	"encoding/json"
	"io"
	"os"
)

type district struct {
	list []models.DistrictDomain
	obj  map[string]models.DistrictDomain
}

func (o district) List(req *models.DistrictFilter) []*models.DistrictDto {
	res := []*models.DistrictDto{}
	for _, v := range o.list {
		if v.Province == req.Province_Eq {
			res = append(res, &models.DistrictDto{ID: v.ID, Name: v.Name})
		}
	}
	return res
}

func (o district) Get(id string) models.BaseDistrictDto {
	if val, ok := o.obj[id]; ok {
		return val.BaseDto()
	}
	return models.BaseDistrictDto{}
}

var District district

func init() {
	async.New().Go(func() {
		jsonList, _ := os.Open("json/district-list.json")
		defer jsonList.Close()
		byteList, _ := io.ReadAll(jsonList)
		json.Unmarshal(byteList, &District.list)
		District.obj = make(map[string]models.DistrictDomain)
		for _, v := range District.list {
			District.obj[v.ID] = v
		}
	})
}
