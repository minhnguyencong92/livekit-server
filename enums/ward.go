package enums

import (
	"app/async"
	"app/models"
	"encoding/json"
	"io"
	"os"
)

type ward struct {
	list []models.WardDomain
	obj  map[string]models.WardDomain
}

func (o ward) List(req *models.WardFilter) []*models.WardDto {
	res := []*models.WardDto{}
	for _, v := range o.list {
		if v.District == req.District_Eq {
			res = append(res, &models.WardDto{ID: v.ID, Name: v.Name})
		}
	}
	return res
}

func (o ward) Get(id string) models.BaseWardDto {
	if val, ok := o.obj[id]; ok {
		return val.BaseDto()
	}
	return models.BaseWardDto{}
}

var Ward ward

func init() {
	async.New().Go(func() {
		jsonList, _ := os.Open("json/ward-list.json")
		defer jsonList.Close()
		byteList, _ := io.ReadAll(jsonList)
		json.Unmarshal(byteList, &Ward.list)
		Ward.obj = make(map[string]models.WardDomain)
		for _, v := range Ward.list {
			Ward.obj[v.ID] = v
		}
	})
}
