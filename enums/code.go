package enums

import (
	"app/models"
	"net/http"
)

type code struct {
	Success             map[string]models.CodeMsg
	SocketConnected     map[string]models.CodeMsg
	Error               map[string]models.CodeMsg
	Unauthorized        map[string]models.CodeMsg
	SignatureInvalid    map[string]models.CodeMsg
	Forbidden           map[string]models.CodeMsg
	BadRequest          map[string]models.CodeMsg
	ApiNotFound         map[string]models.CodeMsg
	TooManyRequest      map[string]models.CodeMsg
	FileNotFound        map[string]models.CodeMsg
	FileTooLarge        map[string]models.CodeMsg
	FileUnsupport       map[string]models.CodeMsg
	CodeInvalid         map[string]models.CodeMsg
	CodeExpired         map[string]models.CodeMsg
	OldPasswordInvalid  map[string]models.CodeMsg
	UserPasswordInvalid map[string]models.CodeMsg
	ClientNotFound      map[string]models.CodeMsg
	UserNotFound        map[string]models.CodeMsg
	UserConflict        map[string]models.CodeMsg
	RoleNotFound        map[string]models.CodeMsg
	RoleConflict        map[string]models.CodeMsg
	GroupNotFound       map[string]models.CodeMsg
	GroupConflict       map[string]models.CodeMsg
	AccountNotFound     map[string]models.CodeMsg
	AccountConflict     map[string]models.CodeMsg
	RoomNotFound        map[string]models.CodeMsg
	ActionNotFound      map[string]models.CodeMsg
	RoomExists          map[string]models.CodeMsg
}

var Code = code{
	Success: map[string]models.CodeMsg{
		"EN": {Status: http.StatusOK, Code: "Success", Message: "Success"},
		"VI": {Status: http.StatusOK, Code: "Success", Message: "Thành công"},
	},
	SocketConnected: map[string]models.CodeMsg{
		"EN": {Status: http.StatusOK, Code: "SocketConnected", Message: "Socket connected"},
		"VI": {Status: http.StatusOK, Code: "SocketConnected", Message: "Socket đã kết nối"},
	},
	Error: map[string]models.CodeMsg{
		"EN": {Status: http.StatusInternalServerError, Code: "Error", Message: "Oops, something went wrong"},
		"VI": {Status: http.StatusInternalServerError, Code: "Error", Message: "Rất tiếc, đã xảy ra lỗi"},
	},
	Unauthorized: map[string]models.CodeMsg{
		"EN": {Status: http.StatusUnauthorized, Code: "Unauthorized", Message: "Unauthorized"},
		"VI": {Status: http.StatusUnauthorized, Code: "Unauthorized", Message: "Unauthorized"},
	},
	SignatureInvalid: map[string]models.CodeMsg{
		"EN": {Status: http.StatusUnauthorized, Code: "SignatureInvalid", Message: "Signature invalid"},
		"VI": {Status: http.StatusUnauthorized, Code: "SignatureInvalid", Message: "Chữ ký không đúng"},
	},
	Forbidden: map[string]models.CodeMsg{
		"EN": {Status: http.StatusForbidden, Code: "Forbidden", Message: "Forbidden"},
		"VI": {Status: http.StatusForbidden, Code: "Forbidden", Message: "Không có quyền truy cập"},
	},
	BadRequest: map[string]models.CodeMsg{
		"EN": {Status: http.StatusBadRequest, Code: "BadRequest", Message: "Bad request"},
		"VI": {Status: http.StatusBadRequest, Code: "BadRequest", Message: "Dữ liệu không hợp lệ"},
	},
	ApiNotFound: map[string]models.CodeMsg{
		"EN": {Status: http.StatusNotFound, Code: "ApiNotFound", Message: "Api not found"},
		"VI": {Status: http.StatusNotFound, Code: "ApiNotFound", Message: "Api không tìm thấy"},
	},
	TooManyRequest: map[string]models.CodeMsg{
		"EN": {Status: http.StatusTooManyRequests, Code: "TooManyRequest", Message: "Too many requests"},
		"VI": {Status: http.StatusTooManyRequests, Code: "TooManyRequest", Message: "Số lần yêu cầu quá nhiều"},
	},
	FileNotFound: map[string]models.CodeMsg{
		"EN": {Status: http.StatusNotFound, Code: "FileNotFound", Message: "File not found"},
		"VI": {Status: http.StatusNotFound, Code: "FileNotFound", Message: "Tệp không tìm thấy"},
	},
	FileTooLarge: map[string]models.CodeMsg{
		"EN": {Status: http.StatusBadRequest, Code: "FileTooLarge", Message: "File too large"},
		"VI": {Status: http.StatusBadRequest, Code: "FileTooLarge", Message: "Tệp quá lớn"},
	},
	FileUnsupport: map[string]models.CodeMsg{
		"EN": {Status: http.StatusUnsupportedMediaType, Code: "FileUnsupport", Message: "File unsupport"},
		"VI": {Status: http.StatusUnsupportedMediaType, Code: "FileUnsupport", Message: "Tệp không được hỗ trợ"},
	},
	CodeInvalid: map[string]models.CodeMsg{
		"EN": {Status: http.StatusBadRequest, Code: "CodeInvalid", Message: "Code invalid"},
		"VI": {Status: http.StatusBadRequest, Code: "CodeInvalid", Message: "Mã xác thực không hợp lệ"},
	},
	CodeExpired: map[string]models.CodeMsg{
		"EN": {Status: http.StatusBadRequest, Code: "CodeExpired", Message: "Code expired"},
		"VI": {Status: http.StatusBadRequest, Code: "CodeExpired", Message: "Mã xác thực hết hạn"},
	},
	OldPasswordInvalid: map[string]models.CodeMsg{
		"EN": {Status: http.StatusBadRequest, Code: "OldPasswordInvalid", Message: "Old password invalid"},
		"VI": {Status: http.StatusBadRequest, Code: "OldPasswordInvalid", Message: "Mật khẩu cũ không hợp lệ"},
	},
	UserPasswordInvalid: map[string]models.CodeMsg{
		"EN": {Status: http.StatusBadRequest, Code: "UserPasswordInvalid", Message: "User or password invalid"},
		"VI": {Status: http.StatusBadRequest, Code: "UserPasswordInvalid", Message: "Tài khoản hoặc mật khẩu không đúng"},
	},
	ClientNotFound: map[string]models.CodeMsg{
		"EN": {Status: http.StatusNotFound, Code: "ClientNotFound", Message: "Client not found"},
		"VI": {Status: http.StatusNotFound, Code: "ClientNotFound", Message: "Client không tìm thấy"},
	},
	UserNotFound: map[string]models.CodeMsg{
		"EN": {Status: http.StatusNotFound, Code: "UserNotFound", Message: "User not found"},
		"VI": {Status: http.StatusNotFound, Code: "UserNotFound", Message: "Tài khoản không tìm thấy"},
	},
	UserConflict: map[string]models.CodeMsg{
		"EN": {Status: http.StatusConflict, Code: "UserConflict", Message: "User conflict"},
		"VI": {Status: http.StatusConflict, Code: "UserConflict", Message: "Tài khoản đã tồn tại"},
	},
	RoleNotFound: map[string]models.CodeMsg{
		"EN": {Status: http.StatusNotFound, Code: "RoleNotFound", Message: "Role not found"},
		"VI": {Status: http.StatusNotFound, Code: "RoleNotFound", Message: "Vai trò không tìm thấy"},
	},
	RoleConflict: map[string]models.CodeMsg{
		"EN": {Status: http.StatusConflict, Code: "RoleConflict", Message: "Role conflict"},
		"VI": {Status: http.StatusConflict, Code: "RoleConflict", Message: "Vai trò đã tồn tại"},
	},
	GroupNotFound: map[string]models.CodeMsg{
		"EN": {Status: http.StatusNotFound, Code: "GroupNotFound", Message: "Group not found"},
		"VI": {Status: http.StatusNotFound, Code: "GroupNotFound", Message: "Nhóm không tìm thấy"},
	},
	GroupConflict: map[string]models.CodeMsg{
		"EN": {Status: http.StatusConflict, Code: "GroupConflict", Message: "Group conflict"},
		"VI": {Status: http.StatusConflict, Code: "GroupConflict", Message: "Nhóm đã tồn tại"},
	},
	AccountNotFound: map[string]models.CodeMsg{
		"EN": {Status: http.StatusNotFound, Code: "AccountNotFound", Message: "Account not found"},
		"VI": {Status: http.StatusNotFound, Code: "AccountNotFound", Message: "Tài khoản không tìm thấy"},
	},
	AccountConflict: map[string]models.CodeMsg{
		"EN": {Status: http.StatusConflict, Code: "AccountConflict", Message: "Account conflict"},
		"VI": {Status: http.StatusConflict, Code: "AccountConflict", Message: "Tài khoản đã tồn tại"},
	},
	RoomNotFound: map[string]models.CodeMsg{
		"EN": {Status: http.StatusNotFound, Code: "RoomNotFound", Message: "Room not found"},
		"VI": {Status: http.StatusNotFound, Code: "RoomNotFound", Message: "Phòng không tìm thấy"},
	},
	ActionNotFound: map[string]models.CodeMsg{
		"EN": {Status: http.StatusNotFound, Code: "ActionNotFound", Message: "Action not found"},
		"VI": {Status: http.StatusNotFound, Code: "ActionNotFound", Message: "Hành động không tìm thấy"},
	},
	RoomExists: map[string]models.CodeMsg{
		"EN": {Status: http.StatusConflict, Code: "RoomExists", Message: "Room Exists"},
		"VI": {Status: http.StatusConflict, Code: "RoomExists", Message: "Room đã tồn tại"},
	},
}
