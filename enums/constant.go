package enums

import "app/models"

type constant struct {
	Req        string
	Page       int64
	Limit      int64
	LimitMax   int64
	Vi         string
	LangRegex  string
	OtpPrefix  string
	OtpDefault string
	OtpExpires int64
	FileValid  models.M
}

var Constant = constant{
	Req:        "req",
	Page:       1,
	Limit:      10,
	LimitMax:   1000,
	Vi:         "VI",
	LangRegex:  "^(en|EN|vi|VI)$",
	OtpPrefix:  "test_",
	OtpDefault: "000000",
	OtpExpires: 5,
	FileValid:  models.M{"image": int64(1e6), "video": int64(30e6), "file": int64(30e6)},
}
