package enums

import (
	"app/async"
	"app/models"
	"encoding/json"
	"io"
	"os"
	"strconv"
)

type province struct {
	list []models.ProvinceDomain
	obj  map[string]models.ProvinceDomain
}

func (o province) List(req *models.ProvinceFilter) []*models.ProvinceDto {
	res := []*models.ProvinceDto{}
	for _, v := range o.list {
		if req.IsCity_Eq != "" {
			isCity, _ := strconv.ParseBool(req.IsCity_Eq)
			if v.IsCity == isCity {
				res = append(res, &models.ProvinceDto{ID: v.ID, Name: v.Name})
			}
		} else {
			res = append(res, &models.ProvinceDto{ID: v.ID, Name: v.Name})
		}
	}
	return res
}

func (o province) Get(id string) models.BaseProvinceDto {
	if val, ok := o.obj[id]; ok {
		return val.BaseDto()
	}
	return models.BaseProvinceDto{}
}

var Province province

func init() {
	async.New().Go(func() {
		jsonList, _ := os.Open("json/province-list.json")
		defer jsonList.Close()
		byteList, _ := io.ReadAll(jsonList)
		json.Unmarshal(byteList, &Province.list)
		Province.obj = make(map[string]models.ProvinceDomain)
		for _, v := range Province.list {
			Province.obj[v.ID] = v
		}
	})
}
