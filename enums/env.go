package enums

import (
	"os"
)

type env struct {
	Port                string
	LogLevel            string
	MongoUri            string
	RedisUri            string
	RootUser            string
	RootPass            string
	ClientId            string
	ClientSecret        string
	ClientScope         string
	Production          bool
	MailHost            string
	MailPort            string
	MailUser            string
	MailPass            string
	FirebaseProjectId   string
	FirebaseClientId    string
	FirebaseClientEmail string
	FirebasePrivateKey  string
	MinIOSsl            bool
	MinIOHost           string
	MinIOUser           string
	MinIOPass           string
	MinIOBucket         string
	CdnUrl              string
	LiveKitKey          string
	LiveKitSecret       string
	LiveKitHost         string
}

var Env = env{
	Port:          "3000",
	LogLevel:      "info",
	MongoUri:      "mongodb://user:pass@localhost:27017/database",
	RedisUri:      "redis://:pass@localhost:6379/0",
	RootUser:      "root",
	RootPass:      "123456",
	ClientId:      "client_id",
	ClientSecret:  "client_secret",
	ClientScope:   "client_scope",
	LiveKitKey:    "live_kit_key",
	LiveKitSecret: "live_kit_secret",
}

func init() {
	if os.Getenv("PORT") != "" {
		Env.Port = os.Getenv("PORT")
	}
	if os.Getenv("LOG_LEVEL") != "" {
		Env.LogLevel = os.Getenv("LOG_LEVEL")
	}
	if os.Getenv("MONGO_URI") != "" {
		Env.MongoUri = os.Getenv("MONGO_URI")
	}
	if os.Getenv("REDIS_URI") != "" {
		Env.RedisUri = os.Getenv("REDIS_URI")
	}
	if os.Getenv("ROOT_USER") != "" {
		Env.RootUser = os.Getenv("ROOT_USER")
	}
	if os.Getenv("ROOT_PASS") != "" {
		Env.RootPass = os.Getenv("ROOT_PASS")
	}
	if os.Getenv("CLIENT_ID") != "" {
		Env.ClientId = os.Getenv("CLIENT_ID")
	}
	if os.Getenv("CLIENT_SECRET") != "" {
		Env.ClientSecret = os.Getenv("CLIENT_SECRET")
	}
	if os.Getenv("CLIENT_SCOPE") != "" {
		Env.ClientScope = os.Getenv("CLIENT_SCOPE")
	}
	if os.Getenv("PRODUCTION") != "" {
		Env.Production = os.Getenv("PRODUCTION") == "T"
	}
	if os.Getenv("MAIL_HOST") != "" {
		Env.MailHost = os.Getenv("MAIL_HOST")
	}
	if os.Getenv("MAIL_PORT") != "" {
		Env.MailPort = os.Getenv("MAIL_PORT")
	}
	if os.Getenv("MAIL_USER") != "" {
		Env.MailUser = os.Getenv("MAIL_USER")
	}
	if os.Getenv("MAIL_PASS") != "" {
		Env.MailPass = os.Getenv("MAIL_PASS")
	}
	if os.Getenv("FIREBASE_PROJECT_ID") != "" {
		Env.FirebaseProjectId = os.Getenv("FIREBASE_PROJECT_ID")
	}
	if os.Getenv("FIREBASE_CLIENT_ID") != "" {
		Env.FirebaseClientId = os.Getenv("FIREBASE_CLIENT_ID")
	}
	if os.Getenv("FIREBASE_CLIENT_EMAIL") != "" {
		Env.FirebaseClientEmail = os.Getenv("FIREBASE_CLIENT_EMAIL")
	}
	if os.Getenv("FIREBASE_PRIVATE_KEY") != "" {
		Env.FirebasePrivateKey = os.Getenv("FIREBASE_PRIVATE_KEY")
	}
	if os.Getenv("MINIO_SSL") != "" {
		Env.MinIOSsl = os.Getenv("MINIO_SSL") == "T"
	}
	if os.Getenv("MINIO_HOST") != "" {
		Env.MinIOHost = os.Getenv("MINIO_HOST")
	}
	if os.Getenv("MINIO_USER") != "" {
		Env.MinIOUser = os.Getenv("MINIO_USER")
	}
	if os.Getenv("MINIO_PASS") != "" {
		Env.MinIOPass = os.Getenv("MINIO_PASS")
	}
	if os.Getenv("MINIO_BUCKET") != "" {
		Env.MinIOBucket = os.Getenv("MINIO_BUCKET")
	}
	if os.Getenv("CDN_URL") != "" {
		Env.CdnUrl = os.Getenv("CDN_URL")
	}
	if os.Getenv("LIVE_KIT_KEY") != "" {
		Env.LiveKitKey = os.Getenv("LIVE_KIT_KEY")
	}
	if os.Getenv("LIVE_KIT_SECRET") != "" {
		Env.LiveKitSecret = os.Getenv("LIVE_KIT_SECRET")
	}
	if os.Getenv("LIVE_KIT_HOST") != "" {
		Env.LiveKitHost = os.Getenv("LIVE_KIT_HOST")
	}
}
