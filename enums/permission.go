package enums

type permission struct {
	Root                                                     string
	ViewAll                                                  string
	RoleView, RoleCreate, RoleUpdate, RoleDelete             string
	GroupView, GroupCreate, GroupUpdate, GroupDelete         string
	AccountView, AccountCreate, AccountUpdate, AccountDelete string
}

var Permission = permission{
	Root:          "root",
	ViewAll:       "6249c5b176dc0056aefa40c1",
	RoleView:      "61a05ebe42b85201ab877340",
	RoleCreate:    "61a05ebe42b85201ab877341",
	RoleUpdate:    "61a05ebe42b85201ab877342",
	RoleDelete:    "61a05ebe42b85201ab877343",
	GroupView:     "62499dd176dc0056aefa40b9",
	GroupCreate:   "62499dd176dc0056aefa40ba",
	GroupUpdate:   "62499dd176dc0056aefa40bb",
	GroupDelete:   "62499dd176dc0056aefa40bc",
	AccountView:   "61a05ebe42b85201ab877344",
	AccountCreate: "61a05ebe42b85201ab877345",
	AccountUpdate: "61a05ebe42b85201ab877346",
	AccountDelete: "61a05ebe42b85201ab877347",
}
