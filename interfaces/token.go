package interfaces

import "app/models"

type IRpoToken interface {
	Create(domain *models.TokenDomain) error
	Update(domain *models.TokenDomain) error
	Delete(data *models.TokenData) error
	FindByAccessToken(accessToken string) (*models.TokenDomain, error)
}
