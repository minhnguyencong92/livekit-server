package interfaces

import "app/models"

type IRpoUser interface {
	Create(domain *models.UserDomain) error
	Update(domain *models.UserDomain) error
	FindAll(req *models.Req) ([]*models.UserDomain, error)
	Count(req *models.Req) (int64, error)
	FindById(id string) (*models.UserDomain, error)
	FindByUsername(username string) (*models.UserDomain, error)
	UpsertByUsername(domain *models.UserDomain) error
}
