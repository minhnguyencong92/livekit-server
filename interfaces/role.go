package interfaces

import "app/models"

type IRpoRole interface {
	Create(domain *models.RoleDomain) error
	Update(domain *models.RoleDomain) error
	Delete(domain *models.RoleDomain) error
	FindAll(req *models.Req) ([]*models.RoleDomain, error)
	Count(req *models.Req) (int64, error)
	FindById(id string) (*models.RoleDomain, error)
}

type ISrvRole interface {
	Create(req *models.Req, data *models.RoleData) *models.Res
	Update(req *models.Req, id string, data *models.RoleData) *models.Res
	Delete(req *models.Req, id string) *models.Res
	FindAll(req *models.Req) *models.Res
	FindAllDropdown(req *models.Req) *models.Res
}
