package interfaces

type ISrvWs interface {
	BroadcastMessageToServer(message any)
	BroadcastMessageToUser(id string, message any)
	BroadcastMessageToRoom(id string, message any)
}
