package interfaces

import "app/models"

type IRpoGroup interface {
	Create(domain *models.GroupDomain) error
	Update(domain *models.GroupDomain) error
	Delete(domain *models.GroupDomain) error
	FindAll(req *models.Req) ([]*models.GroupDomain, error)
	Count(req *models.Req) (int64, error)
	FindById(id string) (*models.GroupDomain, error)
}

type ISrvGroup interface {
	Create(req *models.Req, data *models.GroupData) *models.Res
	Update(req *models.Req, id string, data *models.GroupData) *models.Res
	Delete(req *models.Req, id string) *models.Res
	FindAll(req *models.Req) *models.Res
	FindAllDropdown(req *models.Req) *models.Res
}
