package interfaces

import "app/models"

type IRpoNotify interface {
	Create(domain *models.NotifyDomain) error
	FindAll(req *models.Req) ([]*models.NotifyDomain, error)
	Count(req *models.Req) (int64, error)
	Read(id, receiver string) error
	Delete(id, receiver string) error
}

type ISrvNotify interface {
	Read(req *models.Req, id string) *models.Res
	Delete(req *models.Req, id string) *models.Res
	FindAll(req *models.Req) *models.Res
}
