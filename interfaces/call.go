package interfaces

import "app/models"

type ISrvCall interface {
	MakeCall(req *models.Req, identity string, roomName string) *models.Res
	GetToken(req *models.Req, roomName string, identity string) *models.Res
	CreateRoom(req *models.Req, roomName string) *models.Res
	ListRoom(req *models.Req) *models.Res
	ListParticipants(req *models.Req, roomName string) *models.Res
	LeaveRoom(req *models.Req, roomName string, identity string) *models.Res
}
