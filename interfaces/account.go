package interfaces

import "app/models"

type IRpoAccount interface {
	Create(domain *models.AccountDomain) error
	Update(domain *models.AccountDomain) error
	Delete(domain *models.AccountDomain) error
	FindAll(req *models.Req) ([]*models.AccountDomain, error)
	Count(req *models.Req) (int64, error)
	FindById(id string) (*models.AccountDomain, error)
	GetMe(userId string) (*models.AccountGetMeDomain, error)
	DistinctUserByRole(roleId string) ([]string, error)
}

type ISrvAccount interface {
	Create(req *models.Req, data *models.AccountData) *models.Res
	Update(req *models.Req, id string, data *models.AccountData) *models.Res
	Delete(req *models.Req, id string) *models.Res
	FindAll(req *models.Req) *models.Res
	ResetPass(req *models.Req, id string) *models.Res
}
