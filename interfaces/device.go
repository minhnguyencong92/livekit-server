package interfaces

import "app/models"

type IRpoDevice interface {
	Create(domain *models.DeviceDomain) error
	Update(domain *models.DeviceDomain) error
	FindAll(req *models.Req) ([]*models.DeviceDomain, error)
	Count(req *models.Req) (int64, error)
	FindByDevice(device string) (*models.DeviceDomain, error)
	DeleteByToken(token string) error
	DeleteByDevice(device string) error
}

type ISrvDevice interface {
	Upsert(req *models.Req, device string, data *models.DeviceData) *models.Res
	Delete(req *models.Req, device string) *models.Res
}
