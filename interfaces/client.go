package interfaces

import "app/models"

type IRpoClient interface {
	FindById(id string) (*models.ClientDomain, error)
	FindByClientId(clientId string) (*models.ClientDomain, error)
}
