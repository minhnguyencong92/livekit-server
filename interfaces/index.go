package interfaces

type ILib struct {
	Db         ILibDb
	Mail       ILibMail
	Middleware ILibMiddleware
	Notify     ILibNotify
	Oauth      ILibOauth
	Rdb        ILibRdb
	Storage    ILibStorage
	Util       ILibUtil
	Call       ILibCall
}

type IRpo struct {
	Account IRpoAccount
	Client  IRpoClient
	Device  IRpoDevice
	Group   IRpoGroup
	Notify  IRpoNotify
	Role    IRpoRole
	Token   IRpoToken
	User    IRpoUser
}

type ISrv struct {
	Account ISrvAccount
	Device  ISrvDevice
	Group   ISrvGroup
	Notify  ISrvNotify
	Oauth   ISrvOauth
	Role    ISrvRole
	Ws      ISrvWs
	Call    ISrvCall
}
