package interfaces

import (
	"app/models"
	"io"

	"github.com/go-redis/redis/v8"
	"github.com/gofiber/fiber/v2"
	"github.com/livekit/protocol/livekit"
	"github.com/minio/minio-go/v7"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/gridfs"
)

type ILibDb interface {
	Indexes()
	Migrate()
	Bucket() *gridfs.Bucket
	Device() *mongo.Collection
	Token() *mongo.Collection
	User() *mongo.Collection
	Notify() *mongo.Collection
}

type ILibMail interface {
	SendPassword(data *models.MailPassword) error
}

type ILibMiddleware interface {
	Request() fiber.Handler
	NotFound() fiber.Handler
	Error() fiber.ErrorHandler
	Cors() fiber.Handler
	Helmet() fiber.Handler
	Compress() fiber.Handler
	RequestId() fiber.Handler
	Recover() fiber.Handler
	Logger() fiber.Handler
	Cache() fiber.Handler
	Limiter(...models.Limiter) fiber.Handler
	Validate(c *fiber.Ctx, data any) *models.Res
}

type ILibNotify interface {
	System(receivers []string, data *models.NotifyPush)
}

type ILibOauth interface {
	Token() fiber.Handler
	Authenticate(scopes ...string) fiber.Handler
	Authorize(permissions ...string) fiber.Handler
	Thirdparty(scopes ...string) fiber.Handler
}

type ILibRdb interface {
	Del(keys ...string)
	Dels(prefix string)
	Keys(prefix string) []string
	Get(key string, value any) error
	Publish(key string, value any) error
	Subscribe(keys ...string) *redis.PubSub
	SessionSet(accessToken string, value *models.OauthSessionDto) error
	SessionGet(accessToken string) (*models.OauthSessionDto, error)
	ClientGet(clientId string) (*models.ClientCache, error)
}

type ILibStorage interface {
	Download(filename string) (*minio.Object, error)
	Upload(filename string, reader io.Reader, size int64, contentType string) (*minio.UploadInfo, error)
}

type ILibUtil interface {
	BuildReq(req *models.Req)
	Validate(data any) error
	GeneratorSecret() string
	Encrypt(data, secret string) string
	Decrypt(encrypted, secret string) string
	RandomString(rs *models.RandomString) string
	CreateSignature(data, secret string) string
	VerifySignature(data, secret, signature string) bool
	Convert(src, des any) error
	Index(slice, item any) int
	Includes(slice, item any) bool
	Any(slice any, fn func(e any) bool) bool
	All(slice any, fn func(e any) bool) bool
	Find(slice any, fn func(e any) bool) any
	Map(sliceSrc, sliceDes any, fn func(e any) any) error
	Filter(sliceSrc, sliceDes any, fn func(e any) bool) error
	MapIDToOID(id string) *primitive.ObjectID
	MapIDsToOIDs(ids []string) []*primitive.ObjectID
	AddHostCdn(content string) string
	RemoveHostCdn(content string) string
}

type ILibCall interface {
	CreateRoom(roomName string) (*livekit.Room, error)
	TokenJoinGroup(roomName string, identity string) (string, error)
	ListRooms() (*livekit.ListRoomsResponse, error)
	DeleteRoom(roomId string) (*livekit.DeleteRoomResponse, error)
	ListParticipantsInRoom(roomName string) (*livekit.ListParticipantsResponse, error)
	RemoveParticipant(roomName string, identity string) (*livekit.RemoveParticipantResponse, error)
	IsExistRoom(roomName string) (bool, error)
	GetRoom(roomName string) (*livekit.Room, error)
}
