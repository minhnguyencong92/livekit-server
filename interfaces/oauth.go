package interfaces

import "app/models"

type ISrvOauth interface {
	Login(req *models.Req, body *models.OauthLoginBody) *models.Res
	Register(req *models.Req, body *models.OauthRegisterBody) *models.Res
	RegisterOtp(req *models.Req, body *models.OauthRegisterOtpBody) *models.Res
	ForgotPassword(req *models.Req, body *models.OauthForgotPasswordBody) *models.Res
	ForgotPasswordOtp(req *models.Req, body *models.OauthForgotPasswordOtpBody) *models.Res
	ChangePassword(req *models.Req, body *models.OauthChangePasswordBody) *models.Res
	DeleteMe(req *models.Req, body *models.OauthDeleteBody) *models.Res
	UpdateMe(req *models.Req, data *models.UserMeData) *models.Res
}
