FROM golang:1.18.1-alpine as build
WORKDIR /app
COPY ./async/ ./async/
COPY ./clog/ ./clog/
COPY ./docs/ ./docs/
COPY ./enums/ ./enums/
COPY ./interfaces/ ./interfaces/
COPY ./libs/ ./libs/
COPY ./models/ ./models/
COPY ./modules/ ./modules/
COPY ./routes/ ./routes/
COPY ./go.mod ./
COPY ./go.sum ./
COPY ./main.go ./
RUN go mod download
RUN go build -o /app.bin

FROM golang:1.18.1-alpine
WORKDIR /app
COPY ./public/ ./public/
COPY ./views/ ./views/
COPY ./json ./json/
COPY --from=build /app.bin /app.bin
ENTRYPOINT ["/app.bin"]