package async

import (
	"fmt"
	"sync"
)

type as struct {
	wg  sync.WaitGroup
	one sync.Once
	err error
}

func (o *as) Wait() error {
	o.wg.Wait()
	return o.err
}

func (o *as) Go(fn func()) *as {
	o.wg.Add(1)
	go func() {
		defer o.wg.Done()
		defer func() {
			if r := recover(); r != nil {
				o.one.Do(func() {
					o.err = fmt.Errorf("%v", r)
				})
			}
		}()
		fn()
	}()
	return o
}

func New() *as {
	return &as{}
}
