package models

type MailPassword struct {
	Website  string
	Username string
	Email    string
	Password string
	Lang     string
}
