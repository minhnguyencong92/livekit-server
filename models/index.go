package models

import (
	"time"
)

type M map[string]any

type CLog struct {
	Struct any
	Func   any
	Data   any
	Method any
	Url    any
	Error  any
}

type Req struct {
	Lang    string           `json:"lang,omitempty"`
	Skip    int64            `json:"skip,omitempty"`
	Page    int64            `json:"page,omitempty"`
	Limit   int64            `json:"limit,omitempty"`
	Sorts   []string         `json:"sorts,omitempty"`
	Data    any              `json:"data,omitempty"`
	Sort    M                `json:"sort,omitempty"`
	Filters []M              `json:"filters,omitempty"`
	Session *OauthSessionDto `json:"session,omitempty"`
}

type Res struct {
	CodeMsg
	Total int64 `json:"total,omitempty"`
	Data  any   `json:"data,omitempty"`
	Error any   `json:"error,omitempty"`
}

type CodeMsg struct {
	Status  int    `json:"-"`
	Code    string `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

type Limiter struct {
	Max      int64
	Duration time.Duration
}

type ConfigDto struct {
	CdnUrl string `json:"cdnUrl"`
}

type RandomString struct {
	Length   int
	IsString bool
	IsNumber bool
}
