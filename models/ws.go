package models

import "time"

type WsMessageData struct {
	Action  string `json:"action" validate:"required"`
	Payload any    `json:"payload" validate:"required"`
}

type WsPubSub struct {
	Room    string `json:"room" validate:"required"`
	User    string `json:"user" validate:"required"`
	Message any    `json:"message" validate:"required"`
}

// == WsJoinRoom ===//

type WsJoinRoomPayload struct {
	Room string `json:"room" validate:"required"`
}

type WsJoinRoomData struct {
	Action  string            `json:"action" validate:"required"`
	Payload WsJoinRoomPayload `json:"payload" validate:"required"`
}

type WsJoinRoomDto struct {
	Action    string            `json:"action"`
	Payload   WsJoinRoomPayload `json:"payload"`
	Sender    BaseUserDto       `json:"sender"`
	CreatedAt time.Time         `json:"createdAt"`
}

// == WsLeaveRoom ===//

type WsLeaveRoomPayload struct {
	Room string `json:"room" validate:"required"`
}

type WsLeaveRoomData struct {
	Action  string             `json:"action" validate:"required"`
	Payload WsLeaveRoomPayload `json:"payload" validate:"required"`
}

type WsLeaveRoomDto struct {
	Action    string             `json:"action"`
	Payload   WsLeaveRoomPayload `json:"payload"`
	Sender    BaseUserDto        `json:"sender"`
	CreatedAt time.Time          `json:"createdAt"`
}

// == WsSendMessage ===//

type WsSendMessagePayload struct {
	Room    string `json:"room" validate:"required"`
	Content string `json:"content" validate:"required"`
}

type WsSendMessageData struct {
	Action  string               `json:"action" validate:"required"`
	Payload WsSendMessagePayload `json:"payload" validate:"required"`
}

type WsSendMessageDto struct {
	Action    string               `json:"action"`
	Payload   WsSendMessagePayload `json:"payload"`
	Sender    BaseUserDto          `json:"sender"`
	CreatedAt time.Time            `json:"createdAt"`
}
