package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type AccountDomain struct {
	ID        primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name      string             `json:"name" bson:"name"`
	Phone     string             `json:"phone" bson:"phone"`
	Email     string             `json:"email" bson:"email"`
	Username  string             `json:"username" bson:"username"`
	Avatar    string             `json:"avatar" bson:"avatar"`
	Status    string             `json:"status" bson:"status"`
	Role      string             `json:"role" bson:"role"`
	Groups    []string           `json:"groups" bson:"groups"`
	User      string             `json:"user" bson:"user"`
	IsRoot    bool               `json:"isRoot" bson:"isRoot"`
	CreatedBy string             `json:"createdBy" bson:"createdBy"`
	UpdatedBy string             `json:"updatedBy" bson:"updatedBy"`
	CreatedAt time.Time          `json:"createdAt" bson:"createdAt"`
	UpdatedAt time.Time          `json:"updatedAt" bson:"updatedAt"`
}

func (o AccountDomain) BaseDto() BaseAccountDto {
	return BaseAccountDto{ID: o.ID.Hex(), Name: o.Name, Avatar: o.Avatar}
}

type AccountGetMeDomain struct {
	ID          string   `json:"id" bson:"_id,omitempty"`
	Name        string   `json:"name" bson:"name"`
	Groups      []string `json:"groups" bson:"groups"`
	IsRoot      bool     `json:"isRoot" bson:"isRoot"`
	Username    string   `json:"username" bson:"username"`
	Permissions []string `json:"permissions" bson:"permissions"`
}

type AccountDto struct {
	ID        string         `json:"id"`
	Name      string         `json:"name"`
	Phone     string         `json:"phone"`
	Email     string         `json:"email"`
	Username  string         `json:"username"`
	Avatar    string         `json:"avatar"`
	Status    BaseMetaDto    `json:"status"`
	Role      BaseRoleDto    `json:"role"`
	Groups    []BaseGroupDto `json:"groups"`
	CreatedBy BaseAccountDto `json:"createdBy"`
	UpdatedBy BaseAccountDto `json:"updatedBy"`
	CreatedAt time.Time      `json:"createdAt"`
	UpdatedAt time.Time      `json:"updatedAt"`
}

type AccountData struct {
	Name     string   `json:"name" validate:"required"`
	Phone    string   `json:"phone" validate:"required"`
	Email    string   `json:"email" validate:"required,email"`
	Username string   `json:"username" validate:"required"`
	Avatar   string   `json:"avatar" validate:"omitempty"`
	Status   string   `json:"status" validate:"required,len=24"`
	Role     string   `json:"role" validate:"required,len=24"`
	Groups   []string `json:"groups" validate:"required,min=1,dive,len=24"`
}

func (o AccountData) Domain(domain *AccountDomain) {
	domain.Name = o.Name
	domain.Phone = o.Phone
	domain.Email = o.Email
	domain.Username = o.Username
	domain.Avatar = o.Avatar
	domain.Status = o.Status
	domain.Role = o.Role
	domain.Groups = o.Groups
}

type AccountFilter struct {
	Keywords string `query:"keywords"`
}
