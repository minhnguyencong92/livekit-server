package models

type DistrictDomain struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Province string `json:"province"`
}

func (o DistrictDomain) BaseDto() BaseDistrictDto {
	return BaseDistrictDto{ID: o.ID, Name: o.Name}
}

type DistrictDto struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type DistrictFilter struct {
	Province_Eq string `query:"province_eq"`
}
