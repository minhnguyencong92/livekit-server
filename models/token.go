package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type TokenDomain struct {
	ID          primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	AccessToken string             `json:"accessToken" bson:"accessToken"`
	ExpiresAt   time.Time          `json:"expiresAt" bson:"expiresAt"`
	Scopes      []string           `json:"scopes" bson:"scopes"`
	Client      string             `json:"client" bson:"client"`
	User        string             `json:"user" bson:"user"`
	CreatedAt   time.Time          `json:"createdAt" bson:"createdAt"`
	UpdatedAt   time.Time          `json:"updatedAt" bson:"updatedAt"`
}

type TokenData struct {
	AccessToken, UserId string
	IsDeleteDB          bool
}
