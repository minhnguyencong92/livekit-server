package models

type WardDomain struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	District string `json:"district"`
}

func (o WardDomain) BaseDto() BaseWardDto {
	return BaseWardDto{ID: o.ID, Name: o.Name}
}

type WardDto struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type WardFilter struct {
	District_Eq string `query:"district_eq"`
}
