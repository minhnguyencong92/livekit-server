package models

type MetaDomain struct {
	ID       string `json:"id"`
	Kind     string `json:"kind"`
	Priority int64  `json:"priority"`
	Name     M      `json:"name"`
	Opts     M      `json:"opts"`
}

func (o MetaDomain) BaseDto(lang string) BaseMetaDto {
	return BaseMetaDto{ID: o.ID, Name: o.Name[lang].(string)}
}

type MetaDto struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Opts M      `json:"opts,omitempty"`
}
