package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type NotifyDomain struct {
	ID          primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Title       string             `json:"title" bson:"title"`
	Content     string             `json:"content" bson:"content"`
	Description string             `json:"description" bson:"description"`
	Sender      string             `json:"sender" bson:"sender"`
	Receivers   []string           `json:"receivers" bson:"receivers"`
	Unreaders   []string           `json:"unreaders" bson:"unreaders"`
	Kind        string             `json:"kind" bson:"kind"`
	Group       string             `json:"group" bson:"group"`
	Opts        NotifyOpts         `json:"opts" bson:"opts"`
	CreatedAt   time.Time          `json:"createdAt" bson:"createdAt,omitempty"`
	UpdatedAt   time.Time          `json:"updatedAt" bson:"updatedAt,omitempty"`
}

type NotifyDto struct {
	ID          string      `json:"id"`
	Title       string      `json:"title"`
	Content     string      `json:"content"`
	Description string      `json:"description"`
	IsRead      bool        `json:"isRead"`
	Sender      BaseUserDto `json:"sender"`
	Kind        BaseMetaDto `json:"kind"`
	Group       BaseMetaDto `json:"group"`
	Opts        NotifyOpts  `json:"opts"`
	CreatedAt   time.Time   `json:"createdAt"`
	UpdatedAt   time.Time   `json:"updatedAt"`
}

type NotifyFilter struct {
	Group_Eq string `query:"group_eq"`
}

type NotifyPush struct {
	Title       string
	Description string
	Sender      string
	Opts        NotifyOpts
}

type NotifyOpts struct {
	Name string `json:"name"`
}
