package models

type ProvinceDomain struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	IsCity bool   `json:"isCity"`
}

func (o ProvinceDomain) BaseDto() BaseProvinceDto {
	return BaseProvinceDto{ID: o.ID, Name: o.Name}
}

type ProvinceDto struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type ProvinceFilter struct {
	IsCity_Eq string `query:"iscity_eq"`
}
