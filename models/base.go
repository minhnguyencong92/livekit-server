package models

type BaseProvinceDto struct {
	ID   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type BaseDistrictDto struct {
	ID   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type BaseWardDto struct {
	ID   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type BaseMetaDto struct {
	ID   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type BaseRoleDto struct {
	ID   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type BaseGroupDto struct {
	ID   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type BaseAccountDto struct {
	ID     string `json:"id,omitempty"`
	Name   string `json:"name,omitempty"`
	Avatar string `json:"avatar,omitempty"`
}

type BaseUserDto struct {
	ID     string `json:"id,omitempty"`
	Name   string `json:"name,omitempty"`
	Avatar string `json:"avatar,omitempty"`
}
