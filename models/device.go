package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DeviceDomain struct {
	ID        primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Lang      string             `json:"lang" bson:"lang"`
	Device    string             `json:"device" bson:"device"`
	User      string             `json:"user" bson:"user"`
	Os        string             `json:"os" bson:"os"`
	Token     string             `json:"token" bson:"token"`
	VerCode   int64              `json:"verCode" bson:"verCode"`
	VerName   string             `json:"verName" bson:"verName"`
	CreatedAt time.Time          `json:"createdAt" bson:"createdAt"`
	UpdatedAt time.Time          `json:"updatedAt" bson:"updatedAt"`
}

type DeviceData struct {
	Os      string `json:"os" validate:"required"`
	Token   string `json:"token" validate:"required"`
	VerCode int64  `json:"verCode" validate:"required"`
	VerName string `json:"verName" validate:"required"`
}

func (o DeviceData) Domain(domain *DeviceDomain) {
	domain.Os = o.Os
	domain.Token = o.Token
	domain.VerCode = o.VerCode
	domain.VerName = o.VerName
}
