package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ClientDomain struct {
	ID           primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	ClientId     string             `json:"clientId" bson:"clientId"`
	ClientSecret string             `json:"clientSecret" bson:"clientSecret"`
	Grants       []string           `json:"grants" bson:"grants"`
	Scopes       []string           `json:"scopes" bson:"scopes"`
	Expires      time.Duration      `json:"expires" bson:"expires"`
	CreatedAt    time.Time          `json:"createdAt" bson:"createdAt"`
	UpdatedAt    time.Time          `json:"updatedAt" bson:"updatedAt"`
}

type ClientCache struct {
	ID           string
	ClientId     string
	ClientSecret string
	Grants       []string
	Scopes       []string
	Expires      time.Duration
}
