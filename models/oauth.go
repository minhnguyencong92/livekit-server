package models

type OauthSessionDto struct {
	ID          string   `json:"id"`
	Name        string   `json:"name"`
	Phone       string   `json:"phone"`
	Email       string   `json:"email"`
	Username    string   `json:"username"`
	Avatar      string   `json:"avatar"`
	IsRoot      bool     `json:"isRoot"`
	GroupIDs    []string `json:"groupIds"`
	AccountID   string   `json:"accountId"`
	AccountName string   `json:"accountName"`
	Permissions []string `json:"permissions,omitempty"`
	ClientId    string   `json:"clientId,omitempty"`
	AccessToken string   `json:"accessToken,omitempty"`
	Scopes      []string `json:"scopes,omitempty"`
	Website     string   `json:"website"`
}

func (o OauthSessionDto) BaseDto() BaseUserDto {
	return BaseUserDto{ID: o.ID, Name: o.Name, Avatar: o.Avatar}
}

type OauthClientBody struct {
	Username  string `json:"username" form:"username" validate:"required"`
	Password  string `json:"password" form:"password" validate:"required"`
	GrantType string `json:"grant_type" form:"grant_type" validate:"required"`
}

type OauthTokenBody struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int64  `json:"expires_in"`
	Scope       string `json:"scope"`
}

type OauthLoginBody struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type OauthRegisterBody struct {
	Name     string `json:"name" validate:"required"`
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type OauthRegisterOtpBody struct {
	Username string `json:"username" validate:"required"`
	Code     string `json:"code" validate:"required"`
}

type OauthForgotPasswordBody struct {
	Username string `json:"username" validate:"required"`
}

type OauthForgotPasswordOtpBody struct {
	Username string `json:"username" validate:"required"`
	Code     string `json:"code" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type OauthChangePasswordBody struct {
	Username    string
	OldPassword string `json:"oldPassword" validate:"required"`
	NewPassword string `json:"newPassword" validate:"required"`
}

type OauthDeleteBody struct {
	UserId      string
	AccessToken string
}

type OauthThirdpartyBody struct {
	ClientId  string `json:"clientId" validate:"required"`
	Data      string `json:"data" validate:"required"`
	Signature string `json:"signature" validate:"required"`
}
