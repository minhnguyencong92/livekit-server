package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type GroupDomain struct {
	ID        primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name      string             `json:"name" bson:"name"`
	CreatedBy string             `json:"createdBy" bson:"createdBy"`
	UpdatedBy string             `json:"updatedBy" bson:"updatedBy"`
	CreatedAt time.Time          `json:"createdAt" bson:"createdAt"`
	UpdatedAt time.Time          `json:"updatedAt" bson:"updatedAt"`
}

func (o GroupDomain) BaseDto() BaseGroupDto {
	return BaseGroupDto{ID: o.ID.Hex(), Name: o.Name}
}

type GroupDto struct {
	ID        string         `json:"id"`
	Name      string         `json:"name"`
	CreatedBy BaseAccountDto `json:"createdBy"`
	UpdatedBy BaseAccountDto `json:"updatedBy"`
	CreatedAt time.Time      `json:"createdAt"`
	UpdatedAt time.Time      `json:"updatedAt"`
}

type GroupData struct {
	Name string `json:"name" validate:"required"`
}

func (o GroupData) Domain(domain *GroupDomain) {
	domain.Name = o.Name
}

type GroupFilter struct {
	Keywords string `query:"keywords"`
}
