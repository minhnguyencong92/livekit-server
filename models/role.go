package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type RoleDomain struct {
	ID          primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name        string             `json:"name" bson:"name"`
	Permissions []string           `json:"permissions" bson:"permissions"`
	CreatedBy   string             `json:"createdBy" bson:"createdBy"`
	UpdatedBy   string             `json:"updatedBy" bson:"updatedBy"`
	CreatedAt   time.Time          `json:"createdAt" bson:"createdAt"`
	UpdatedAt   time.Time          `json:"updatedAt" bson:"updatedAt"`
}

func (o RoleDomain) BaseDto() BaseRoleDto {
	return BaseRoleDto{ID: o.ID.Hex(), Name: o.Name}
}

type RoleDto struct {
	ID          string         `json:"id"`
	Name        string         `json:"name"`
	Permissions []BaseMetaDto  `json:"permissions"`
	CreatedBy   BaseAccountDto `json:"createdBy"`
	UpdatedBy   BaseAccountDto `json:"updatedBy"`
	CreatedAt   time.Time      `json:"createdAt"`
	UpdatedAt   time.Time      `json:"updatedAt"`
}

type RoleData struct {
	Name        string   `json:"name" validate:"required"`
	Permissions []string `json:"permissions" validate:"required,min=1,dive,len=24"`
}

func (o RoleData) Domain(domain *RoleDomain) {
	domain.Name = o.Name
	domain.Permissions = o.Permissions
}

type RoleFilter struct {
	Keywords string `query:"keywords"`
}
