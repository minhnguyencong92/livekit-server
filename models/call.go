package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type CreateRoomRequest struct {
	// RoomName string `json:"roomName" validate:"required"`
	Identity string `json:"identity" validate:"required"`
	RoomName string `json:"roomName" validate:"omitempty"`
}

type TokenJoinRoomRequest struct {
	RoomName string `json:"roomName" validate:"required"`
	Identity string `json:"identity" validate:"required"`
}

type CallRoom struct {
	RoomID   string `json:"roomId"`
	RoomName string `json:"roomName"`
}

type MakeCall struct {
	Room  CallRoom `json:"room"`
	Token string   `json:"token"`
}

type CallToken struct {
	Token string `json:"token"`
}

type MakeCallRequest struct {
	RoomName string `json:"roomName"`
}

type CallParticipant struct {
	Name     string `json:"name"`
	ID       string `json:"id"`
	Identity string `json:"identity"`
}

type CallType string

const (
	CallTypePrivate CallType = "PRIVATE"
	CallTypeGroup   CallType = "GROUP"
)

// type CallStatus string

// const (
// 	CallStatusSuccess CallStatus = "success"
// 	CallStatusBusy    CallStatus = "busy"
// 	CallStatusMissing CallStatus = "missing"
// )

type KeyVal struct {
	Key, Val string
}

type callstatus struct {
	CallStatusSuccess string
	CallStatusBusy    string
	CallStatusMissing string
}

var CallStatus = callstatus{
	CallStatusSuccess: "success",
	CallStatusBusy:    "busy",
	CallStatusMissing: "missing",
}

type MemberCallDomain struct {
	UserID     string    `json:"userId" bson:"userId"`
	StartTime  time.Time `json:"startTime" bson:"startTime"`
	EndTime    time.Time `json:"endTime" bson:"endTime"`
	CallStatus string    `json:"callStatus" bson:"callStatus"`
}

type CallDomain struct {
	ID       primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	RoomID   string             `json:"roomId" bson:"roomId"`
	Owner    string             `json:"owner" bson:"owner"`
	Members  []MemberCallDomain `json:"members" bson:"members"`
	CallType CallType           `json:"callType" bson:"callType"`
}

type CallEventType string

const (
	CallEventTypeAck     CallEventType = "ACK"
	CallEventTypeAnswer  CallEventType = "ANSWER"
	CallEventTypeHangup  CallEventType = "HANG_UP"
	CallEventTypeBusy    CallEventType = "BUSY"
	CallEventTypeOnGoing CallEventType = "ON_GOING"
	CallEventTypeTimeOut CallEventType = "TIMEOUT"
)

type CallEvent struct {
	Type   CallEventType `json:"type"`
	CallID string        `json:"callId"`
}

type CallRequest struct {
	CallID     string `json:"callId"`
	ReceiverID string `json:"receiverId"`
}
