package models

type SwagCodeMsg struct {
	CodeMsg
}

type SwagConfigDto struct {
	CodeMsg
	Data ConfigDto
}

type SwagAccountDtoList struct {
	CodeMsg
	Total int64
	Data  []AccountDto
}

type SwagDistrictDtoList struct {
	CodeMsg
	Data []DistrictDto
}

type SwagGroupDtoList struct {
	CodeMsg
	Total int64
	Data  []GroupDto
}

type SwagGroupBaseDtoList struct {
	CodeMsg
	Data []BaseGroupDto
}

type SwagMetaDto struct {
	CodeMsg
	Data map[string][]MetaDto
}

type SwagNotifyDtoList struct {
	CodeMsg
	Total int64
	Data  []NotifyDto
}

type SwagOauthTokenBody struct {
	CodeMsg
	Data OauthTokenBody
}

type SwagOauthSessionDto struct {
	CodeMsg
	Data OauthSessionDto
}

type SwagProvinceDtoList struct {
	CodeMsg
	Data []ProvinceDto
}

type SwagRoleDtoList struct {
	CodeMsg
	Total int64
	Data  []RoleDto
}

type SwagRoleBaseDtoList struct {
	CodeMsg
	Data []BaseRoleDto
}

type SwagWardDtoList struct {
	CodeMsg
	Data []WardDto
}

type SwagWsJoinRoomDto struct {
	CodeMsg
	Data WsJoinRoomDto
}

type SwagWsLeaveRoomDto struct {
	CodeMsg
	Data WsLeaveRoomDto
}

type SwagWsSendMessageDto struct {
	CodeMsg
	Data WsSendMessageDto
}
