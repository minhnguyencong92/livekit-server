package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UserDomain struct {
	ID         primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name       string             `json:"name" bson:"name"`
	Phone      string             `json:"phone" bson:"phone"`
	Email      string             `json:"email" bson:"email"`
	Username   string             `json:"username" bson:"username"`
	Birthday   string             `json:"birthday" bson:"birthday"`
	Address    string             `json:"address" bson:"address"`
	Avatar     string             `json:"avatar" bson:"avatar"`
	Gender     string             `json:"gender" bson:"gender"`
	Status     string             `json:"status" bson:"status"`
	Secret     string             `json:"secret" bson:"secret"`
	Password   string             `json:"password" bson:"password"`
	OtpCode    string             `json:"otpCode" bson:"otpCode"`
	OtpExpires time.Time          `json:"otpExpires" bson:"otpExpires"`
	IsRoot     bool               `json:"isRoot" bson:"isRoot"`
	CreatedAt  time.Time          `json:"createdAt" bson:"createdAt"`
	UpdatedAt  time.Time          `json:"updatedAt" bson:"updatedAt"`
}

func (o UserDomain) BaseDto() BaseUserDto {
	return BaseUserDto{ID: o.ID.Hex(), Name: o.Name, Avatar: o.Avatar}
}

type UserMeData struct {
	ID       string `json:"id"`
	Name     string `json:"name" validate:"required"`
	Phone    string `json:"phone" validate:"required"`
	Email    string `json:"email" validate:"required"`
	Birthday string `json:"birthday" validate:"required"`
	Address  string `json:"address" validate:"required"`
	Avatar   string `json:"avatar" validate:"required"`
	Gender   string `json:"gender" validate:"required,len=24"`
}

func (o UserMeData) Domain(domain *UserDomain) {
	domain.Name = o.Name
	domain.Phone = o.Phone
	domain.Email = o.Email
	domain.Birthday = o.Birthday
	domain.Address = o.Address
	domain.Avatar = o.Avatar
	domain.Gender = o.Gender
}
