import axios from 'axios'
import i18n from './i18n'
import store from './store'

const http = axios.create({ baseURL: process.env.VUE_APP_API })
http.interceptors.request.use(async config => {
	const token = await store.dispatch('fnGetToken')
	config.headers['Authorization'] = `${token.token_type} ${token.access_token}`
	config.headers['Accept-Language'] = i18n.locale
	config.headers['Content-Type'] = 'application/json'
	return config
}, err => Promise.reject(err))
http.interceptors.response.use(res => res, err => {
	if (err.response && err.response.status == 401) return store.dispatch('fnLogout')
	return Promise.reject(err.response ? err.response.data : { code: 500, message: err.message })
})

export const upload = async ({ host, url, file, files }) => {
	try {
		http.defaults.baseURL = host || process.env.VUE_APP_API
		const body = new FormData()
		if (file) body.append('file', file)
		else if (files) for (const f of files) body.append('files', f)
		const { data } = await http.post(url, body, { headers: { 'Content-Type': 'multipart/form-data' } })
		return data
	} catch (err) { return err }
}

export const post = async ({ host, url, body }) => {
	try {
		http.defaults.baseURL = host || process.env.VUE_APP_API
		const { data } = await http.post(url, body)
		return data
	} catch (err) { return err }
}

export const put = async ({ host, url, body }) => {
	try {
		http.defaults.baseURL = host || process.env.VUE_APP_API
		const { data } = await http.put(url, body)
		return data
	} catch (err) { return err }
}

export const get = async ({ host, url }) => {
	try {
		http.defaults.baseURL = host || process.env.VUE_APP_API
		const { data } = await http.get(url)
		return data
	} catch (err) { return err }
}

export const del = async ({ host, url }) => {
	try {
		http.defaults.baseURL = host || process.env.VUE_APP_API
		const { data } = await http.delete(url)
		return data
	} catch (err) { return err }
}
