import Vue from 'vue'
import Vuex from 'vuex'
import * as uuid from 'uuid'
import * as Api from './api'
import router from './router'
import { fnCopy, fnBuildQuery } from './utils'
import i18n from './i18n'

Vue.use(Vuex)

const models = [
	'Login', 'Forgot', 'Profile', 'Meta', 'Province', 'District', 'Ward', 'ListFrom', 'ListTo',
	'Role', 'Group', 'Account',
	'RoleAccount', 'GroupAccount',
]

const opt = {
	obj: {},
	list: [],
	total: 0,
	isForm: false,
	isCreate: false,
	isClone: false,
	isUpdate: false,
	isDelete: false,
	isRefresh: false,
	isLoading: false,
	itemPages: [5, 10, 50, 100, 500, 1000],
	filter: { page: 1, limit: 10, sorts: 'createdAt:-1' }
}

const rules = {
	required: v => !!v || i18n.messages[i18n.locale].rule.required,
	number: v => Number(v) >= 0 || i18n.messages[i18n.locale].rule.number,
	phone: v => v ? /^((09|08|07|05|03)+([0-9]{8})|(0101+([0-9]{6})))$\b/.test(v) || i18n.messages[i18n.locale].rule.phone : true,
	minPass: v => (!!v && v.length >= 5) || i18n.messages[i18n.locale].rule.minPass,
	array: v => (v && !!v.length) || i18n.messages[i18n.locale].rule.required,
	email: v => v ? /.+@.+/.test(v) || i18n.messages[i18n.locale].rule.email : true,
}

const permission = {
	VIEW_ALL: '6249c5b176dc0056aefa40c1',
	ROLE_VIEW: "61a05ebe42b85201ab877340",
	ROLE_CREATE: "61a05ebe42b85201ab877341",
	ROLE_UPDATE: "61a05ebe42b85201ab877342",
	ROLE_DELETE: "61a05ebe42b85201ab877343",
	GROUP_VIEW: "62499dd176dc0056aefa40b9",
	GROUP_CREATE: "62499dd176dc0056aefa40ba",
	GROUP_UPDATE: "62499dd176dc0056aefa40bb",
	GROUP_DELETE: "62499dd176dc0056aefa40bc",
	ACCOUNT_VIEW: "61a05ebe42b85201ab877344",
	ACCOUNT_CREATE: "61a05ebe42b85201ab877345",
	ACCOUNT_UPDATE: "61a05ebe42b85201ab877346",
	ACCOUNT_DELETE: "61a05ebe42b85201ab877347",
}

export const state = {
	Api,
	Host: process.env.VUE_APP_API,
	HostCdn: "",
	Token: {},
	Session: {},
	Notifications: [],
	IsLogin: false,
	Rules: { ...rules },
	...models.reduce((o, k) => ({ ...o, [k]: fnCopy(opt) }), {}),
	...permission,
}

export const actions = {
	fnGetToken: ({ state }) => {
		if (!state.IsLogin && localStorage.getItem('token')) {
			try { state.Token = JSON.parse(localStorage.getItem('token')) }
			catch (_) { }
		}
		return state.Token
	},
	fnSetToken: async ({ state, dispatch }, data) => {
		state.Token = data
		localStorage.setItem('token', JSON.stringify(data))
		await dispatch('fnGetMe')
	},
	fnGetConfig: async ({ state, dispatch }) => {
		if (state.HostCdn) return
		const { code, data } = await Api.get({ url: '/v1/static/config' })
		if (code == 'Success') {
			state.HostCdn = data.cdnUrl
			await dispatch('fnGets', { url: '/v1/static/meta', model: 'Meta' })
		}
	},
	fnGetMe: async ({ state, dispatch }) => {
		await dispatch('fnGetToken')
		await dispatch('fnGetConfig')
		if (!state.Token.access_token) return
		if (Object.keys(state.Session).length) return state.Session
		const { code, data } = await Api.get({ url: '/v1/oauth/me' })
		if (code === 'Success') {
			state.IsLogin = true
			state.Session = data
		}
	},
	fnLogout: async ({ state }) => {
		await Api.del({ url: `/v1/oauth/me/${state.Session.id}?authorization=${state.Token.access_token}` })
		state.Token = {}
		state.Session = {}
		state.IsLogin = false
		localStorage.clear()
		router.push('/login')
	},
	fnGets: async ({ state, dispatch }, { host, url, model }) => {
		state[model].isLoading = true
		const { code, data, message, total } = await Api.get({ host, url: `${url}?${fnBuildQuery(state[model].filter)}` })
		if (code === 'Success') {
			if (Array.isArray(data)) {
				state[model].list = data
				state[model].total = total
			} else state[model].obj = data
		}
		else dispatch('fnNotificationView', { type: code === 'Success' ? 'success' : 'error', message })
		state[model].isLoading = false
		return { code, data, total }
	},
	fnPosts: async ({ state, dispatch }, { host, url, model }) => {
		state[model].isLoading = true
		const { code, data, message, total } = await Api.post({ host, url: `${url}?${fnBuildQuery(state[model].filter)}`, body: state[model].obj })
		if (code === 'Success') {
			if (Array.isArray(data)) {
				state[model].list = data
				state[model].total = total
			} else state[model].obj = data
		}
		else dispatch('fnNotificationView', { type: code === 'Success' ? 'success' : 'error', message })
		state[model].isLoading = false
		return { code, data, total }
	},
	fnGet: async ({ state, dispatch }, { host, url, model }) => {
		state[model].isLoading = true
		const { code, data, message } = await Api.get({ host, url })
		if (code === 'Success') state[model].obj = data
		else dispatch('fnNotificationView', { type: code === 'Success' ? 'success' : 'error', message })
		state[model].isLoading = false
		return { code, data }
	},
	fnPost: async ({ state, dispatch }, { host, url, model }) => {
		state[model].isLoading = true
		const { code, data, message } = await Api.post({ host, url, body: state[model].obj })
		dispatch('fnNotificationView', { type: code === 'Success' ? 'success' : 'error', message })
		state[model].isLoading = false
		return { code, data }
	},
	fnPut: async ({ state, dispatch }, { host, url, model }) => {
		state[model].isLoading = true
		const { code, data, message } = await Api.put({ host, url, body: state[model].obj })
		dispatch('fnNotificationView', { type: code === 'Success' ? 'success' : 'error', message })
		state[model].isLoading = false
		return { code, data }
	},
	fnDelete: async ({ state, dispatch }, { host, url, model }) => {
		state[model].isLoading = true
		const { code, data, message } = await Api.del({ host, url })
		dispatch('fnNotificationView', { type: code === 'Success' ? 'success' : 'error', message })
		state[model].isLoading = false
		return { code, data }
	},
	fnResetFlagView: ({ state }, model) => {
		state[model] = { ...state[model], obj: {}, isForm: false, isValid: false, isCreate: false, isClone: false, isUpdate: false, isDelete: false, isRefresh: false, isLoading: false }
	},
	fnCallListView: ({ dispatch }, { url, model }) => {
		dispatch('fnResetFlagView', model)
		dispatch('fnGets', { url, model })
	},
	fnCallCreateView: ({ dispatch, state }, model) => {
		dispatch('fnResetFlagView', model)
		state[model].isForm = true
		state[model].isCreate = true
	},
	fnCallCloneView: ({ dispatch, state }, { model, item }) => {
		dispatch('fnResetFlagView', model)
		state[model].isForm = true
		state[model].isClone = true
		state[model].obj = fnCopy(item)
	},
	fnCallUpdateView: ({ dispatch, state }, { model, item }) => {
		dispatch('fnResetFlagView', model)
		state[model].isForm = true
		state[model].isUpdate = true
		state[model].obj = fnCopy(item)
	},
	fnCallDeleteView: ({ dispatch, state }, { model, item }) => {
		dispatch('fnResetFlagView', model)
		state[model].isDelete = true
		state[model].obj = fnCopy(item)
	},
	fnNotificationView: ({ state }, { type, message }) => {
		const noti = { type, message, key: uuid.v4() }
		state.Notifications.push(noti)
		setTimeout(() => { state.Notifications = state.Notifications.filter(e => e.key != noti.key) }, 3000)
	}
}

export default new Vuex.Store({ actions, state })
