export const fnCopy = data => JSON.parse(JSON.stringify(data))

export const fnCheckPermission = (session, permissions = []) => {
	return permissions.some(e => session.permissions.includes(e))
}

export const fnBuildQuery = (filter) => {
	return Object.keys(filter).reduce((a, k) => [...a, `${k}=${Array.isArray(filter[k]) ? filter[k].join(',') : filter[k]}`], []).join('&')
}

export const fnFormatNumber = (val) => {
	if (!val) return 0
	val = val.toString()
	const format = (n) => n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
	if (val.indexOf(".") >= 0) {
		const pos = val.indexOf(".")
		const left = format(val.substring(0, pos))
		const right = format(val.substring(pos).substring(0, 3))
		return left + "." + (right.length == 2 ? right : (right + '0'))
	} else {
		return format(val)
	}
}

export const fnFormatPercent = (val) => {
	if (!val) return 0
	val = val.toString()
	const format = (n) => n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
	if (val.indexOf(".") >= 0) {
		const pos = val.indexOf(".")
		const left = format(val.substring(0, pos))
		const right = format(val.substring(pos).substring(0, 3))
		return left + "." + (right.length == 2 ? right : (right + '0'))
	} else {
		return format(val) + '.00'
	}
}

export const fnNumberInput = (obj, field, val) => {
	if (!val) return
	obj[field] = fnFormatNumber(val)
}

export const fnToNumber = (val) => {
	if (!val) return 0
	return Number(val.toString().replace(/,/g, ""))
}

export const fnEventNumber = (evt) => {
	evt = evt ? evt : window.event
	var charCode = evt.which ? evt.which : evt.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
		evt.preventDefault()
	} else {
		return true
	}
}