import Vue from 'vue'
import Vuetify from 'vuetify'
import VueMoment from 'vue-moment'
import CKEditor from 'ckeditor4-vue'
import VueClipboard from 'vue-clipboard2'
import Viewer from 'v-viewer'
import { mapState, mapActions } from 'vuex'
import { fnCopy, fnCheckPermission, fnBuildQuery, fnFormatNumber, fnFormatPercent, fnNumberInput, fnEventNumber, fnToNumber } from './utils'
import router from './router'
import store, { actions, state } from './store'
import App from './App.vue'
import colors from 'vuetify/es5/util/colors'
import 'viewerjs/dist/viewer.css'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'
import i18n from './i18n'

Vue.use(Viewer)
Vue.use(Vuetify)
Vue.use(VueMoment)
Vue.use(CKEditor)
Vue.use(VueClipboard)

import Editor from './components/Editor'
Vue.component('v-editor', Editor)
import Tag from './components/Tag'
Vue.component('v-tag', Tag)
import Chart from './components/Chart'
Vue.component('v-chart', Chart)
import Upload from './components/Upload'
Vue.component('v-upload', Upload)
import Button from './components/Button'
Vue.component('v-btnc', Button)
import AceEditor from './components/AceEditor'
Vue.component('v-ace-editor', AceEditor)
import Province from './components/Province'
Vue.component('v-province', Province)
import District from './components/District'
Vue.component('v-district', District)
import Ward from './components/Ward'
Vue.component('v-ward', Ward)
import RoleAccount from './components/RoleAccount'
Vue.component('v-roleAccount', RoleAccount)
import GroupAccount from './components/GroupAccount'
Vue.component('v-groupAccount', GroupAccount)
import StatusAccount from './components/StatusAccount'
Vue.component('v-statusAccount', StatusAccount)
import Notification from './components/Notification'
Vue.component('v-notification', Notification)
import TimePicker from "./components/TimePicker";
Vue.component("v-timePicker", TimePicker);
import DatePicker from './components/DatePicker'
Vue.component('v-datePicker', DatePicker)
import DateTimePicker from "./components/DateTimePicker";
Vue.component("v-dateTimePicker", DateTimePicker);
import DatePickerRange from './components/DatePickerRange'
Vue.component('v-datePickerRange', DatePickerRange)

Vue.filter('mDateTime', v => Vue.moment(v).format('HH:mm:ss DD/MM/YYYY'))
Vue.filter('mDate', v => Vue.moment(v).format('DD/MM/YYYY'))
Vue.filter('mNumber', v => fnFormatNumber(v) || '-')

Vue.mixin({
	computed: mapState(Object.keys(state)),
	methods: {
		fnCopy, fnCheckPermission, fnBuildQuery, fnFormatNumber, fnFormatPercent, fnNumberInput, fnEventNumber, fnToNumber,
		...mapActions(Object.keys(actions))
	}
})

new Vue({
	i18n,
	store,
	router,
	vuetify: new Vuetify({ theme: { themes: { light: { primary: colors.teal, secondary: colors.grey } } } }),
	render: h => h(App)
}).$mount('#app')
