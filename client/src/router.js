import Vue from 'vue'
import Router from 'vue-router'

import store from './store'
import { fnCheckPermission } from './utils'

import Home from './pages/Home'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Profile from './pages/Profile'
import Role from './pages/role/Role'
import Group from './pages/group/Group'
import Account from './pages/account/Account'

Vue.use(Router)

const router = new Router({
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home,
			props: {
				auth: true
			}
		},
		{
			path: '/role',
			name: 'role',
			component: Role,
			props: {
				auth: true,
				permissions: [store.state.ROLE_VIEW]
			}
		},
		{
			path: '/group',
			name: 'group',
			component: Group,
			props: {
				auth: true,
				permissions: [store.state.GROUP_VIEW]
			}
		},
		{
			path: '/account',
			name: 'account',
			component: Account,
			props: {
				auth: true,
				permissions: [store.state.ACCOUNT_VIEW]
			}
		},
		{
			path: '/login',
			name: 'login',
			component: Login,
			props: {
				auth: false
			}
		},
		{
			path: '/logout',
			name: 'logout',
			component: Logout,
			props: {
				auth: true
			}
		},
		{
			path: '/profile',
			name: 'profile',
			component: Profile,
			props: {
				auth: true
			}
		},
		{
			path: '*',
			redirect: '/',
			props: {
				auth: true
			}
		}
	]
})

router.beforeEach(async (to, from, next) => {
	await store.dispatch('fnGetMe')
	const { auth, permissions } = to.matched[0].props.default
	if (auth) {
		if (!store.state.Token.access_token) return store.dispatch('fnLogout')
		if (permissions && !fnCheckPermission(store.state.Session, permissions)) {
			return next('/')
		}
	} else if (store.state.IsLogin) {
		return next('/')
	}
	next()
})

export default router
