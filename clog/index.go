package clog

import (
	"app/enums"
	"app/models"
	"reflect"

	"github.com/sirupsen/logrus"
)

func init() {
	logrus.SetFormatter(&logrus.TextFormatter{ForceColors: true, FullTimestamp: true})
	logrusLevel, _ := logrus.ParseLevel(enums.Env.LogLevel)
	logrus.SetLevel(logrusLevel)
}

func Fatal(value ...any) {
	logrus.Fatalln(value...)
}

func Error(value ...any) {
	logrus.Errorln(value...)
}

func Warn(value ...any) {
	logrus.Warnln(value...)
}

func Info(value ...any) {
	logrus.Infoln(value...)
}

func Errorf(format string, value ...any) {
	logrus.Errorf(format, value...)
}

func Warnf(format string, value ...any) {
	logrus.Warnf(format, value...)
}

func Infof(format string, value ...any) {
	logrus.Infof(format, value...)
}

func Errorc(data *models.CLog) {
	logrus.Errorln(buildDatas(data)...)
}

func Warnc(data *models.CLog) {
	logrus.Warnln(buildDatas(data)...)
}

func Infoc(data *models.CLog) {
	logrus.Infoln(buildDatas(data)...)
}

func buildDatas(data *models.CLog) []any {
	datas := []any{}
	if data.Struct != nil {
		datas = append(datas, "[Struct]:", reflect.TypeOf(data.Struct))
	}
	if data.Func != nil {
		datas = append(datas, "- [Func]:", data.Func)
	}
	if data.Data != nil {
		datas = append(datas, "- [Data]:", data.Data)
	}
	if data.Method != nil {
		datas = append(datas, "- [Method]:", data.Method)
	}
	if data.Url != nil {
		datas = append(datas, "- [Url]:", data.Url)
	}
	if data.Error != nil {
		datas = append(datas, "- [Error]:", data.Error)
	}
	return datas
}
