package libs

import (
	"app/clog"
	"app/enums"
	"app/interfaces"
	"app/models"
	"encoding/base64"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

type oauth struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
}

func NewOauth(rpo *interfaces.IRpo, lib *interfaces.ILib) interfaces.ILibOauth {
	return oauth{rpo: rpo, lib: lib}
}

func (o oauth) Token() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)

		authorization := c.Get(fiber.HeaderAuthorization)
		if !strings.HasPrefix(authorization, "Basic ") {
			res := models.Res{CodeMsg: enums.Code.Unauthorized[req.Lang]}
			return c.Status(res.Status).JSON(res)
		}

		base64Basic := strings.Split(authorization, " ")[1]
		byteBasic, err := base64.StdEncoding.DecodeString(base64Basic)
		if err != nil {
			res := models.Res{CodeMsg: enums.Code.Error[req.Lang], Error: err.Error()}
			return c.Status(res.Status).JSON(res)
		}

		arrayBasic := strings.Split(string(byteBasic), ":")
		if len(arrayBasic) != 2 {
			res := models.Res{CodeMsg: enums.Code.Unauthorized[req.Lang]}
			return c.Status(res.Status).JSON(res)
		}

		clientId := arrayBasic[0]
		clientSecret := arrayBasic[1]

		client, err := o.lib.Rdb.ClientGet(clientId)
		if err != nil || client.ClientSecret != clientSecret {
			res := models.Res{CodeMsg: enums.Code.ClientNotFound[req.Lang]}
			return c.Status(res.Status).JSON(res)
		}

		body := models.OauthClientBody{}
		if res := o.lib.Middleware.Validate(c, &body); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		if !o.lib.Util.Includes(client.Grants, body.GrantType) {
			res := models.Res{CodeMsg: enums.Code.BadRequest[req.Lang]}
			return c.Status(res.Status).JSON(res)
		}

		userId := ""
		if body.GrantType == "password" {
			user, err := o.rpo.User.FindByUsername(body.Username)
			if err != nil || user.Status != enums.Meta.StatusUser.Activated || o.lib.Util.Decrypt(user.Password, user.Secret) != body.Password {
				res := models.Res{CodeMsg: enums.Code.UserPasswordInvalid[req.Lang]}
				return c.Status(res.Status).JSON(res)
			}
			userId = user.ID.Hex()
		}

		token := &models.TokenDomain{
			AccessToken: uuid.NewString(),
			ExpiresAt:   time.Now().Add(client.Expires),
			Scopes:      client.Scopes,
			Client:      client.ID,
			User:        userId,
			CreatedAt:   time.Now(),
			UpdatedAt:   time.Now(),
		}
		o.rpo.Token.Create(token)

		res := models.Res{CodeMsg: enums.Code.Success[req.Lang]}
		res.Data = models.OauthTokenBody{
			AccessToken: token.AccessToken,
			TokenType:   "Bearer",
			ExpiresIn:   int64(client.Expires.Seconds()),
			Scope:       strings.Join(client.Scopes, " "),
		}
		return c.Status(res.Status).JSON(res)
	}
}

func (o oauth) Authenticate(scopes ...string) fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)

		scope := enums.Env.ClientScope
		if len(scopes) > 1 {
			scope = scopes[0]
		}

		authorization := c.Get(fiber.HeaderAuthorization)
		if authorization == "" {
			authorization = c.Query(strings.ToLower(fiber.HeaderAuthorization))
		}

		if !strings.HasPrefix(authorization, "Bearer ") {
			res := models.Res{CodeMsg: enums.Code.Unauthorized[req.Lang]}
			return c.Status(res.Status).JSON(res)
		}

		accessToken := strings.Split(authorization, " ")[1]

		session, err := o.lib.Rdb.SessionGet(accessToken)
		if err == nil && o.lib.Util.Includes(session.Scopes, scope) {
			req.Session = session
			c.Locals(enums.Constant.Req, req)
			return c.Next()
		}

		token, err := o.rpo.Token.FindByAccessToken(accessToken)
		if err != nil || !o.lib.Util.Includes(token.Scopes, scope) {
			res := models.Res{CodeMsg: enums.Code.Unauthorized[req.Lang]}
			return c.Status(res.Status).JSON(res)
		}

		if token.ExpiresAt.Before(time.Now()) {
			o.rpo.Token.Delete(&models.TokenData{AccessToken: accessToken, IsDeleteDB: true})
			res := models.Res{CodeMsg: enums.Code.Unauthorized[req.Lang]}
			return c.Status(res.Status).JSON(res)
		}

		client, _ := o.rpo.Client.FindById(token.Client)

		token.UpdatedAt = time.Now()
		token.ExpiresAt = time.Now().Add(client.Expires)
		o.rpo.Token.Update(token)

		session = &models.OauthSessionDto{
			ClientId:    client.ClientId,
			AccessToken: token.AccessToken,
			Scopes:      token.Scopes,
			Website:     c.BaseURL(),
		}

		if token.User != "" {
			if user, err := o.rpo.User.FindById(token.User); err == nil {
				session.ID = user.ID.Hex()
				session.Name = user.Name
				session.Phone = user.Phone
				session.Email = user.Email
				session.Avatar = user.Avatar
			}
			if account, err := o.rpo.Account.GetMe(token.User); err == nil {
				session.AccountID = account.ID
				session.AccountName = account.Name
				session.GroupIDs = account.Groups
				session.IsRoot = account.IsRoot
				session.Username = account.Username
				session.Permissions = account.Permissions
			}
		}

		if err := o.lib.Rdb.SessionSet(accessToken, session); err != nil {
			clog.Infoc(&models.CLog{Struct: oauth{}, Func: "Authenticate()", Error: err.Error()})
			res := models.Res{CodeMsg: enums.Code.Error[req.Lang], Error: err.Error()}
			return c.Status(res.Status).JSON(res)
		}

		req.Session = session
		c.Locals(enums.Constant.Req, req)
		return c.Next()
	}
}

func (o oauth) Authorize(permissions ...string) fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)

		if len(permissions) > 0 {
			for _, permission := range permissions {
				if o.lib.Util.Includes(req.Session.Permissions, permission) {
					return c.Next()
				}
			}
		}

		res := models.Res{CodeMsg: enums.Code.Forbidden[req.Lang]}
		return c.Status(res.Status).JSON(res)
	}
}

func (o oauth) Thirdparty(scopes ...string) fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)

		body := models.OauthThirdpartyBody{}
		if res := o.lib.Middleware.Validate(c, &body); res.Error != nil {
			return c.Status(res.Status).JSON(res)
		}

		client, err := o.lib.Rdb.ClientGet(body.ClientId)
		if err != nil {
			res := models.Res{CodeMsg: enums.Code.ClientNotFound[req.Lang]}
			return c.Status(res.Status).JSON(res)
		}

		if !o.lib.Util.VerifySignature(body.Data, client.ClientSecret, body.Signature) {
			res := models.Res{CodeMsg: enums.Code.SignatureInvalid[req.Lang]}
			return c.Status(res.Status).JSON(res)
		}

		byteBody, err := base64.StdEncoding.DecodeString(body.Data)
		if err != nil {
			res := models.Res{CodeMsg: enums.Code.Error[req.Lang], Error: err.Error()}
			return c.Status(res.Status).JSON(res)
		}
		c.Request().SetBody(byteBody)

		req.Session = &models.OauthSessionDto{ClientId: client.ClientId}
		c.Locals(enums.Constant.Req, req)
		return c.Next()
	}
}
