package libs

import (
	"app/clog"
	"app/enums"
	"app/interfaces"
	"context"
	"fmt"
	"io"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

type storage struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	cli *minio.Client
}

func NewStorage(rpo *interfaces.IRpo, lib *interfaces.ILib) interfaces.ILibStorage {
	return storage{rpo: rpo, lib: lib, cli: newStorage()}
}

func newStorage() *minio.Client {
	ctx := context.Background()

	client, err := minio.New(enums.Env.MinIOHost, &minio.Options{
		Creds:  credentials.NewStaticV4(enums.Env.MinIOUser, enums.Env.MinIOPass, ""),
		Secure: enums.Env.MinIOSsl,
	})
	if err != nil {
		clog.Fatal(err)
	}

	ok, err := client.BucketExists(ctx, enums.Env.MinIOBucket)
	if err != nil {
		clog.Fatal(err)
	}
	if !ok {
		clog.Fatal(fmt.Errorf("bucket %v not exists", enums.Env.MinIOBucket))
	}

	fmt.Printf("Storage connected storage://%v:%v@%v/%v\n", enums.Env.MinIOUser, enums.Env.MinIOPass, enums.Env.MinIOHost, enums.Env.MinIOBucket)

	return client
}

func (o storage) Download(filename string) (*minio.Object, error) {
	ctx := context.Background()
	return o.cli.GetObject(ctx, enums.Env.MinIOBucket, filename, minio.GetObjectOptions{})
}

func (o storage) Upload(filename string, reader io.Reader, size int64, contentType string) (*minio.UploadInfo, error) {
	ctx := context.Background()

	info, err := o.cli.PutObject(ctx, enums.Env.MinIOBucket, filename, reader, size, minio.PutObjectOptions{
		CacheControl: "public, max-age=2592000",
		ContentType:  contentType,
	})

	return &info, err
}
