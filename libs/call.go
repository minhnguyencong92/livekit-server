package libs

import (
	"app/enums"
	"app/interfaces"
	"context"
	"time"

	"github.com/livekit/protocol/auth"
	"github.com/livekit/protocol/livekit"
	lksdk "github.com/livekit/server-sdk-go"
)

const (
	EmptyTimeout          = 3600 * 24 * 10
	TokenJoinGroupTimeout = time.Hour * 24 * 180
)

type call struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	cli *lksdk.RoomServiceClient
}

func NewCall(rpo *interfaces.IRpo, lib *interfaces.ILib) interfaces.ILibCall {
	return call{rpo: rpo, lib: lib, cli: newCall()}
}

func newCall() *lksdk.RoomServiceClient {
	client := lksdk.NewRoomServiceClient(enums.Env.LiveKitHost, enums.Env.LiveKitKey, enums.Env.LiveKitSecret)
	return client
}

func (o call) CreateRoom(roomName string) (*livekit.Room, error) {
	return o.cli.CreateRoom(context.Background(), &livekit.CreateRoomRequest{
		Name:         roomName,
		EmptyTimeout: EmptyTimeout,
	})
}

func (o call) TokenJoinGroup(roomName string, identity string) (string, error) {
	at := auth.NewAccessToken(enums.Env.LiveKitKey, enums.Env.LiveKitSecret)

	grant := &auth.VideoGrant{
		RoomJoin: true,
		Room:     roomName}

	at.AddGrant(grant).
		SetIdentity(identity).
		SetValidFor(TokenJoinGroupTimeout)

	return at.ToJWT()
}

func (o call) ListRooms() (*livekit.ListRoomsResponse, error) {
	return o.cli.ListRooms(context.Background(), &livekit.ListRoomsRequest{})
}

func (o call) DeleteRoom(roomId string) (*livekit.DeleteRoomResponse, error) {
	return o.cli.DeleteRoom(context.Background(), &livekit.DeleteRoomRequest{
		Room: roomId,
	})
}

func (o call) RemoveParticipant(roomName string, identity string) (*livekit.RemoveParticipantResponse, error) {
	return o.cli.RemoveParticipant(context.Background(), &livekit.RoomParticipantIdentity{
		Room:     roomName,
		Identity: identity,
	})
}

func (o call) ListParticipantsInRoom(roomName string) (*livekit.ListParticipantsResponse, error) {
	return o.cli.ListParticipants(context.Background(), &livekit.ListParticipantsRequest{
		Room: roomName,
	})
}

func (o call) IsExistRoom(roomName string) (bool, error) {
	rooms, err := o.ListRooms()
	if err != nil {
		return false, err
	}

	for _, a := range rooms.Rooms {
		if roomName == a.Name {
			return true, nil
		}
	}
	return false, nil
}

func (o call) GetRoom(roomName string) (*livekit.Room, error) {
	rooms, err := o.ListRooms()
	if err != nil {
		return nil, err
	}

	for _, a := range rooms.Rooms {
		if roomName == a.Name {
			return a, nil
		}
	}
	return nil, nil
}
