package libs

import (
	"app/clog"
	"app/enums"
	"app/interfaces"
	"app/models"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cache"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/limiter"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/fiber/v2/middleware/requestid"
	"github.com/gofiber/helmet/v2"
)

type middleware struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
}

func NewMiddleware(rpo *interfaces.IRpo, lib *interfaces.ILib) interfaces.ILibMiddleware {
	return middleware{rpo: rpo, lib: lib}
}

func (o middleware) Request() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := models.Req{}
		c.QueryParser(&req)
		req.Lang = strings.ToUpper(c.Get(fiber.HeaderAcceptLanguage))
		o.lib.Util.BuildReq(&req)
		c.Locals(enums.Constant.Req, req)
		return c.Next()
	}
}

func (o middleware) NotFound() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := c.Locals(enums.Constant.Req).(models.Req)
		res := models.Res{CodeMsg: enums.Code.ApiNotFound[req.Lang]}
		return c.Status(res.Status).JSON(res)
	}
}

func (o middleware) Error() fiber.ErrorHandler {
	return func(c *fiber.Ctx, err error) error {
		clog.Errorc(&models.CLog{Struct: middleware{}, Func: "Error()", Method: c.Method(), Url: c.Path(), Error: err.Error()})
		lang := enums.Constant.Vi
		if r := c.Locals(enums.Constant.Req); r != nil {
			lang = r.(models.Req).Lang
		}
		res := models.Res{CodeMsg: enums.Code.Error[lang], Error: err.Error()}
		return c.Status(res.Status).JSON(res)
	}
}

func (o middleware) Cors() fiber.Handler {
	return cors.New()
}

func (o middleware) Helmet() fiber.Handler {
	return helmet.New()
}

func (o middleware) Compress() fiber.Handler {
	return compress.New()
}

func (o middleware) RequestId() fiber.Handler {
	return requestid.New()
}

func (o middleware) Recover() fiber.Handler {
	return recover.New(recover.Config{
		EnableStackTrace: true,
	})
}

func (o middleware) Logger() fiber.Handler {
	return logger.New(logger.Config{
		Next: func(c *fiber.Ctx) bool {
			return enums.Env.Production
		},
	})
}

func (o middleware) Cache() fiber.Handler {
	return cache.New()
}

func (o middleware) Limiter(config ...models.Limiter) fiber.Handler {
	cfg := models.Limiter{}

	if len(config) > 0 {
		cfg = config[0]
	}

	if cfg.Max <= 0 {
		cfg.Max = 20
	}

	if int(cfg.Duration.Seconds()) <= 0 {
		cfg.Duration = 1 * time.Minute
	}

	return limiter.New(limiter.Config{
		Max:        int(cfg.Max),
		Expiration: cfg.Duration,
		LimitReached: func(c *fiber.Ctx) error {
			req := c.Locals(enums.Constant.Req).(models.Req)
			res := models.Res{CodeMsg: enums.Code.TooManyRequest[req.Lang]}
			return c.Status(res.Status).JSON(res)
		},
	})
}

func (o middleware) Validate(c *fiber.Ctx, data any) *models.Res {
	req := c.Locals(enums.Constant.Req).(models.Req)
	res := &models.Res{CodeMsg: enums.Code.BadRequest[req.Lang]}
	var err error

	if c.Method() == fiber.MethodGet {
		err = c.QueryParser(data)
	} else {
		err = c.BodyParser(data)
	}

	if err != nil {
		clog.Warnc(&models.CLog{Struct: middleware{}, Func: "Validate()", Method: c.Method(), Url: c.Path(), Error: err.Error()})
		res.CodeMsg = enums.Code.Error[req.Lang]
		res.Error = err.Error()
		return res
	}

	if err := o.lib.Util.Validate(data); err != nil {
		res.Error = err.Error()
	}

	return res
}
