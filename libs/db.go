package libs

import (
	"app/clog"
	"app/enums"
	"app/interfaces"
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/gridfs"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"go.mongodb.org/mongo-driver/x/mongo/driver/connstring"
)

type db struct {
	lib      *interfaces.ILib
	bucket   *gridfs.Bucket
	database *mongo.Database
}

func NewDb(lib *interfaces.ILib) interfaces.ILibDb {
	bucket, database := newDb()
	return db{lib: lib, bucket: bucket, database: database}
}

func newDb() (*gridfs.Bucket, *mongo.Database) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	conn, err := connstring.ParseAndValidate(enums.Env.MongoUri)
	if err != nil {
		clog.Fatal(err)
	}

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(conn.Original).SetReadPreference(readpref.SecondaryPreferred()))
	if err != nil {
		clog.Fatal(err)
	}

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		clog.Fatal(err)
	}

	database := client.Database(conn.Database)

	bucket, err := gridfs.NewBucket(database)
	if err != nil {
		clog.Fatal(err)
	}

	fmt.Printf("Mongo connected %v\n", enums.Env.MongoUri)

	return bucket, database
}

func (o db) Bucket() *gridfs.Bucket {
	return o.bucket
}

func (o db) Device() *mongo.Collection {
	return o.database.Collection("device")
}

func (o db) Notify() *mongo.Collection {
	return o.database.Collection("notify")
}

func (o db) Token() *mongo.Collection {
	return o.database.Collection("token")
}

func (o db) User() *mongo.Collection {
	return o.database.Collection("user")
}

func (o db) Indexes() {
	ctx := context.TODO()

	o.Token().Indexes().CreateOne(ctx, mongo.IndexModel{
		Keys:    bson.D{{Key: "accessToken", Value: 1}},
		Options: options.Index().SetUnique(true),
	})

	o.User().Indexes().CreateOne(ctx, mongo.IndexModel{
		Keys: bson.D{{Key: "username", Value: 1}},
		Options: options.Index().SetUnique(true).SetPartialFilterExpression(bson.M{
			"username": bson.M{"$exists": true, "$gt": ""},
		}),
	})
	o.User().Indexes().CreateOne(ctx, mongo.IndexModel{
		Keys: bson.D{{Key: "phone", Value: 1}},
		Options: options.Index().SetUnique(true).SetPartialFilterExpression(bson.M{
			"phone": bson.M{"$exists": true, "$gt": ""},
		}),
	})
	o.User().Indexes().CreateOne(ctx, mongo.IndexModel{
		Keys: bson.D{{Key: "email", Value: 1}},
		Options: options.Index().SetUnique(true).SetPartialFilterExpression(bson.M{
			"email": bson.M{"$exists": true, "$gt": ""},
		}),
	})

	// o.Notify().Indexes().CreateOne(ctx, mongo.IndexModel{
	// 	Keys: bson.D{{Key: "group", Value: 1}},
	// })

	o.Device().Indexes().CreateOne(ctx, mongo.IndexModel{
		Keys:    bson.D{{Key: "device", Value: 1}},
		Options: options.Index().SetUnique(true),
	})
}

func (o db) Migrate() {
	// ctx := context.TODO()
	// secret := o.lib.Util.GeneratorSecret()

}
