package libs

import (
	"app/clog"
	"app/enums"
	"app/interfaces"
	"app/models"
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/ReneKroon/ttlcache"
	"github.com/go-redis/redis/v8"
)

type rdb struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	cli *redis.Client
	cac *ttlcache.Cache
}

func NewRdb(rpo *interfaces.IRpo, lib *interfaces.ILib) interfaces.ILibRdb {
	return rdb{rpo: rpo, lib: lib, cli: newRdb(), cac: newCac()}
}

func newRdb() *redis.Client {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	otps, err := redis.ParseURL(enums.Env.RedisUri)
	if err != nil {
		clog.Fatal(err)
	}

	client := redis.NewClient(otps)

	if ping := client.Ping(ctx); ping.Err() != nil {
		clog.Fatal(ping.Err())
	}

	fmt.Printf("Redis connected %v\n", enums.Env.RedisUri)

	return client
}

func newCac() *ttlcache.Cache {
	cac := ttlcache.NewCache()
	cac.SetTTL(10 * time.Second)
	return cac
}

func (o rdb) Del(keys ...string) {
	o.cli.Del(context.TODO(), keys...)
}

func (o rdb) Dels(prefix string) {
	keys, _ := o.cli.Keys(context.TODO(), fmt.Sprintf("%v*", prefix)).Result()
	o.Del(keys...)
}

func (o rdb) Keys(prefix string) []string {
	keys, _ := o.cli.Keys(context.TODO(), fmt.Sprintf("%v*", prefix)).Result()
	return keys
}

func (o rdb) Publish(key string, value any) error {
	bytes, _ := json.Marshal(value)
	return o.cli.Publish(context.TODO(), key, bytes).Err()
}

func (o rdb) Subscribe(keys ...string) *redis.PubSub {
	return o.cli.Subscribe(context.TODO(), keys...)
}

func (o rdb) Set(key string, value any, expiration time.Duration) error {
	bytes, _ := json.Marshal(value)
	return o.cli.Set(context.TODO(), key, bytes, expiration).Err()
}

func (o rdb) Get(key string, value any) error {
	val, err := o.cli.Get(context.TODO(), key).Result()
	if err != nil {
		return err
	}
	return json.Unmarshal([]byte(val), value)
}

func (o rdb) SessionSet(accessToken string, value *models.OauthSessionDto) error {
	return o.Set(accessToken, value, 1*time.Hour)
}

func (o rdb) SessionGet(accessToken string) (*models.OauthSessionDto, error) {
	cache, _ := o.cac.Get(accessToken)
	if cache != nil {
		return cache.(*models.OauthSessionDto), nil
	}

	res := &models.OauthSessionDto{}

	if err := o.Get(accessToken, &res); err != nil {
		return nil, err
	}

	o.cac.Set(accessToken, res)
	return res, nil
}

func (o rdb) ClientGet(clientId string) (*models.ClientCache, error) {
	key := fmt.Sprintf("client_%v", clientId)

	cache, _ := o.cac.Get(key)
	if cache != nil {
		return cache.(*models.ClientCache), nil
	}

	res := &models.ClientCache{}

	if err := o.Get(key, &res); err != nil {
		if obj, err := o.rpo.Client.FindByClientId(clientId); err != nil {
			return nil, err
		} else {
			res.ID = obj.ID.Hex()
			res.ClientId = obj.ClientId
			res.ClientSecret = obj.ClientSecret
			res.Grants = obj.Grants
			res.Scopes = obj.Scopes
			res.Expires = obj.Expires
			o.Set(key, res, 1*time.Hour)
		}
	}

	o.cac.Set(key, res)
	return res, nil
}
