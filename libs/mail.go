package libs

import (
	"app/enums"
	"app/interfaces"
	"app/models"
	"bytes"
	"fmt"
	"html/template"
	"net/smtp"
)

type mail struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
}

func NewMail(rpo *interfaces.IRpo, lib *interfaces.ILib) interfaces.ILibMail {
	return mail{rpo: rpo, lib: lib}
}

func (o mail) SendMail(to, subject, html string) error {
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	msg := "From: Demo <" + enums.Env.MailUser + ">\r\nTo: " + to + "\r\nSubject: " + subject + "\r\n" + mime + "\r\n" + html

	addr := fmt.Sprintf("%v:%v", enums.Env.MailHost, enums.Env.MailPort)
	auth := smtp.PlainAuth("", enums.Env.MailUser, enums.Env.MailPass, enums.Env.MailHost)

	return smtp.SendMail(addr, auth, enums.Env.MailUser, []string{to}, []byte(msg))
}

func (o mail) ParseTemplate(filename string, data any) (string, error) {
	tmpl, err := template.ParseFiles(filename)
	if err != nil {
		return "", err
	}

	buffer := new(bytes.Buffer)
	if err = tmpl.Execute(buffer, data); err != nil {
		return "", err
	}

	return buffer.String(), nil
}

func (o mail) SendPassword(data *models.MailPassword) error {
	html, err := o.ParseTemplate("views/password.html", data)
	if err != nil {
		return err
	}

	subject := "Sign in information"
	if data.Lang == enums.Constant.Vi {
		subject = "Thông tin đăng nhập"
	}

	return o.SendMail(data.Email, subject, html)
}
