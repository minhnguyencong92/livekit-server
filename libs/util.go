package libs

import (
	"app/enums"
	"app/interfaces"
	"app/models"
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	cRand "crypto/rand"
	"crypto/sha1"
	"crypto/subtle"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	mRand "math/rand"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode"

	"github.com/go-playground/validator"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type util struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
}

func NewUtil(rpo *interfaces.IRpo, lib *interfaces.ILib) interfaces.ILibUtil {
	return util{rpo: rpo, lib: lib}
}

func (o util) BuildReq(req *models.Req) {
	if ok, _ := regexp.MatchString(enums.Constant.LangRegex, req.Lang); !ok {
		req.Lang = enums.Constant.Vi
	}

	if req.Page <= 0 {
		req.Page = enums.Constant.Page
	}

	if req.Limit <= 0 || req.Limit > enums.Constant.LimitMax {
		req.Limit = enums.Constant.Limit
	}

	if req.Skip == 0 {
		req.Skip = (req.Page - 1) * req.Limit
	}

	if len(req.Sorts) > 0 {
		req.Sort = models.M{}
		for _, sort := range req.Sorts {
			parts := strings.Split(sort, ":")
			if len(parts) == 2 {
				if val, err := strconv.ParseInt(parts[1], 0, 0); err == nil && (val == 1 || val == -1) {
					req.Sort[parts[0]] = val
				}
			}
		}
	}

	if len(req.Sort) == 0 {
		req.Sort = models.M{"createdAt": -1}
	}
}

func (o util) Validate(data any) error {
	validate := validator.New()
	if err := validate.Struct(data); err != nil {
		result := models.M{}
		for _, e := range err.(validator.ValidationErrors) {
			chars := []rune(e.Field())
			chars[0] = unicode.ToLower(chars[0])
			key := string(chars)
			msgs := []string{e.Type().String(), e.Tag()}
			if e.Param() != "" {
				msgs = append(msgs, e.Param())
			}
			result[key] = strings.Join(msgs, " ")
		}
		bytes, _ := json.Marshal(result)
		return errors.New(string(bytes))
	}
	return nil
}

func (o util) GeneratorSecret() string {
	bytes := make([]byte, 32)
	cRand.Read(bytes)
	return hex.EncodeToString(bytes)
}

func (o util) Encrypt(data, secret string) string {
	key, _ := hex.DecodeString(secret)
	bytes := []byte(data)
	block, _ := aes.NewCipher(key)
	aesGCM, _ := cipher.NewGCM(block)
	nonce := make([]byte, aesGCM.NonceSize())
	io.ReadFull(cRand.Reader, nonce)
	byteEncrypted := aesGCM.Seal(nonce, nonce, bytes, nil)
	return fmt.Sprintf("%x", byteEncrypted)
}

func (o util) Decrypt(encrypted, secret string) string {
	key, _ := hex.DecodeString(secret)
	bytes, _ := hex.DecodeString(encrypted)
	block, _ := aes.NewCipher(key)
	aesGCM, _ := cipher.NewGCM(block)
	nonceSize := aesGCM.NonceSize()
	nonce, ciphertext := bytes[:nonceSize], bytes[nonceSize:]
	byteData, _ := aesGCM.Open(nil, nonce, ciphertext, nil)
	return string(byteData)
}

func (o util) RandomString(rs *models.RandomString) string {
	if rs.Length <= 0 {
		rs.Length = 6
	}

	str1 := ""
	if rs.IsString {
		str1 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	}

	str2 := ""
	if rs.IsNumber {
		str2 = "0123456789"
	}

	letters := []rune(fmt.Sprintf("%v%v", str1, str2))

	if len(letters) < rs.Length {
		return ""
	}

	s := make([]rune, rs.Length)
	rd := mRand.New(mRand.NewSource(time.Now().UnixNano()))

	for i := range s {
		s[i] = letters[rd.Intn(len(letters))]
	}

	return string(s)
}

func (o util) CreateSignature(data, secret string) string {
	mac := hmac.New(sha1.New, []byte(secret))
	mac.Write([]byte(data))
	return hex.EncodeToString(mac.Sum(nil))
}

func (o util) VerifySignature(data, secret, signature string) bool {
	payload := o.CreateSignature(data, secret)
	return subtle.ConstantTimeCompare([]byte(payload), []byte(signature)) == 1
}

func (o util) Convert(src, des any) error {
	bytes, err := json.Marshal(src)
	if err != nil {
		return err
	}
	return json.Unmarshal(bytes, &des)
}

func (o util) Index(slice, item any) int {
	s := reflect.ValueOf(slice)
	for i := 0; i < s.Len(); i++ {
		if s.Index(i).Interface() == item {
			return i
		}
	}
	return -1
}

func (o util) Includes(slice, item any) bool {
	return o.Index(slice, item) >= 0
}

func (o util) Any(slice any, fn func(any) bool) bool {
	s := reflect.ValueOf(slice)
	for i := 0; i < s.Len(); i++ {
		if fn(s.Index(i).Interface()) {
			return true
		}
	}
	return false
}

func (o util) All(slice any, fn func(any) bool) bool {
	s := reflect.ValueOf(slice)
	for i := 0; i < s.Len(); i++ {
		if !fn(s.Index(i).Interface()) {
			return false
		}
	}
	return true
}

func (o util) Find(slice any, fn func(any) bool) any {
	s := reflect.ValueOf(slice)
	for i := 0; i < s.Len(); i++ {
		if fn(s.Index(i).Interface()) {
			return s.Index(i).Interface()
		}
	}
	return nil
}

func (o util) Map(sliceSrc, sliceDes any, fn func(any) any) error {
	s := reflect.ValueOf(sliceSrc)
	ns := make([]any, s.Len())
	for i := 0; i < s.Len(); i++ {
		ns[i] = fn(s.Index(i).Interface())
	}
	return o.Convert(ns, sliceDes)
}

func (o util) Filter(sliceSrc, sliceDes any, fn func(any) bool) error {
	s := reflect.ValueOf(sliceSrc)
	ns := make([]any, 0)
	for i := 0; i < s.Len(); i++ {
		if fn(s.Index(i).Interface()) {
			ns = append(ns, s.Index(i).Interface())
		}
	}
	return o.Convert(ns, sliceDes)
}

func (o util) MapIDToOID(id string) *primitive.ObjectID {
	oid, _ := primitive.ObjectIDFromHex(id)
	return &oid
}

func (o util) MapIDsToOIDs(ids []string) []*primitive.ObjectID {
	oids := []*primitive.ObjectID{}
	for _, id := range ids {
		if oid, err := primitive.ObjectIDFromHex(id); err == nil {
			oids = append(oids, &oid)
		}
	}
	return oids
}

func (o util) AddHostCdn(content string) string {
	r1 := regexp.MustCompile("src=\"/[image|video|file]")
	return r1.ReplaceAllStringFunc(content, func(s string) string {
		r2 := regexp.MustCompile("/").Split(s, -1)
		return fmt.Sprintf("%v%v/%v", r2[0], enums.Env.CdnUrl, r2[1])
	})
}

func (o util) RemoveHostCdn(content string) string {
	r1 := regexp.MustCompile(fmt.Sprintf("src=\"%v/[image|video|file]", enums.Env.CdnUrl))
	return r1.ReplaceAllStringFunc(content, func(s string) string {
		r2 := regexp.MustCompile(fmt.Sprintf("%v/", enums.Env.CdnUrl)).Split(s, -1)
		return fmt.Sprintf("%v/%v", r2[0], r2[1])
	})
}
