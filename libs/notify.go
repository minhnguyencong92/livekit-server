package libs

import (
	"app/clog"
	"app/enums"
	"app/interfaces"
	"app/models"
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"google.golang.org/api/option"
)

type notify struct {
	rpo *interfaces.IRpo
	lib *interfaces.ILib
	cli *messaging.Client
}

func NewNotify(rpo *interfaces.IRpo, lib *interfaces.ILib) interfaces.ILibNotify {
	return notify{rpo: rpo, lib: lib, cli: newNotify()}
}

func newNotify() *messaging.Client {
	ctx := context.Background()

	credential := models.M{
		"type":         "service_account",
		"project_id":   enums.Env.FirebaseProjectId,
		"client_id":    enums.Env.FirebaseClientId,
		"client_email": enums.Env.FirebaseClientEmail,
		"private_key":  enums.Env.FirebasePrivateKey,
	}
	byteCredential, _ := json.Marshal(credential)
	opts := option.WithCredentialsJSON(byteCredential)

	app, err := firebase.NewApp(ctx, &firebase.Config{ProjectID: enums.Env.FirebaseProjectId}, opts)
	if err != nil {
		clog.Fatal(err)
	}

	client, err := app.Messaging(ctx)
	if err != nil {
		clog.Fatal(err)
	}

	fmt.Printf("Firebase connected firebase://%v:%v\n", enums.Env.FirebaseProjectId, enums.Env.FirebaseClientEmail)

	return client
}

func (o notify) Send(token string, dto *models.NotifyDto) {
	byteNotify, _ := json.Marshal(dto)

	msg := &messaging.Message{
		Token:        token,
		Notification: &messaging.Notification{Title: dto.Title, Body: dto.Description},
		APNS:         &messaging.APNSConfig{Payload: &messaging.APNSPayload{Aps: &messaging.Aps{Sound: "default"}}},
		Data: map[string]string{
			"title":        dto.Title,
			"message":      dto.Description,
			"ejson":        string(byteNotify),
			"click_action": "FLUTTER_NOTIFICATION_CLICK",
		},
	}

	if _, err := o.cli.Send(context.TODO(), msg); err != nil {
		clog.Warnc(&models.CLog{Struct: notify{}, Func: "Send()", Data: token, Error: err.Error()})
		o.rpo.Device.DeleteByToken(token)
	}
}

func (o notify) SaveNotify(receivers []string, domain *models.NotifyDomain) {
	domain.Receivers = receivers
	domain.Unreaders = receivers
	domain.CreatedAt = time.Now()
	domain.UpdatedAt = time.Now()
	o.rpo.Notify.Create(domain)

	for _, receiver := range receivers {
		devices, _ := o.rpo.Device.FindAll(&models.Req{Filters: []models.M{{"user": receiver}}})
		for _, device := range devices {
			dto := models.NotifyDto{
				ID:          domain.ID.Hex(),
				Title:       domain.Title,
				Content:     domain.Content,
				Description: domain.Description,
				CreatedAt:   domain.CreatedAt,
				UpdatedAt:   domain.UpdatedAt,
				IsRead:      false,
				Opts:        domain.Opts,
				Group:       enums.Meta.Get(domain.Group, device.Lang),
			}
			if domain.Kind != "" {
				dto.Kind = enums.Meta.Get(domain.Kind, device.Lang)
				dto.Title = strings.Replace(dto.Kind.Name, "{name}", domain.Opts.Name, 1)
			}
			if sender, err := o.rpo.User.FindById(domain.Sender); err == nil {
				dto.Sender = sender.BaseDto()
			}
			o.Send(device.Token, &dto)
		}
	}
}

func (o notify) System(receivers []string, data *models.NotifyPush) {
	o.SaveNotify(receivers, &models.NotifyDomain{
		Group:       enums.Meta.NotifyGroup.System,
		Kind:        enums.Meta.Notify.System,
		Title:       data.Title,
		Description: data.Description,
		Opts:        data.Opts,
		Sender:      data.Sender,
	})
}
