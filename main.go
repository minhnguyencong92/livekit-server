package main

import (
	"app/clog"
	_ "app/docs"
	"app/enums"
	"app/interfaces"
	"app/libs"
	"app/modules/call"
	"app/modules/cdn"
	"app/modules/device"
	"app/modules/notify"
	"app/modules/oauth"
	"app/modules/token"
	"app/modules/user"
	"app/modules/ws"
	"app/routes"
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/swagger"
	"github.com/gofiber/template/html"
)

// @title                       Document APIs
// @securityDefinitions.apikey  Bearer
// @in                          Header
// @name                        Authorization
// @BasePath                    /
func main() {
	rpo := interfaces.IRpo{}
	lib := interfaces.ILib{}
	srv := interfaces.ISrv{}

	rpo.Device = device.NewRepo(&rpo, &lib)
	rpo.Notify = notify.NewRepo(&rpo, &lib)
	rpo.Token = token.NewRepo(&rpo, &lib)
	rpo.User = user.NewRepo(&rpo, &lib)

	// lib.Db = libs.NewDb(&lib)
	// lib.Rdb = libs.NewRdb(&rpo, &lib)
	lib.Util = libs.NewUtil(&rpo, &lib)
	lib.Mail = libs.NewMail(&rpo, &lib)
	lib.Oauth = libs.NewOauth(&rpo, &lib)
	// lib.Notify = libs.NewNotify(&rpo, &lib)
	// lib.Storage = libs.NewStorage(&rpo, &lib)
	lib.Middleware = libs.NewMiddleware(&rpo, &lib)
	lib.Call = libs.NewCall(&rpo, &lib)

	srv.Device = device.NewService(&rpo, &lib, &srv)
	srv.Notify = notify.NewService(&rpo, &lib, &srv)
	srv.Oauth = oauth.NewService(&rpo, &lib, &srv)
	srv.Ws = ws.NewService(&rpo, &lib, &srv)
	srv.Call = call.NewService(&rpo, &lib, &srv)

	// lib.Db.Indexes()
	// lib.Db.Migrate()

	ws.NewServer(&rpo, &lib, &srv)

	app := fiber.New(fiber.Config{
		BodyLimit:    1024 * 1024 * 50,
		ErrorHandler: lib.Middleware.Error(),
		Views:        html.New("./views", ".html"),
	})
	app.Static("/", "./public")
	app.All("/docs/*", swagger.New())
	app.Use(lib.Middleware.Cors())
	app.Use(lib.Middleware.Helmet())
	app.Use(lib.Middleware.Recover())
	app.Use(lib.Middleware.Request())
	app.Mount("/v1", routes.V1(&rpo, &lib, &srv))
	app.Mount(fmt.Sprintf("/%v", enums.Env.MinIOBucket), cdn.NewRouter(&rpo, &lib, &srv))
	app.Use(lib.Middleware.NotFound())
	clog.Fatal(app.Listen(fmt.Sprintf(":%v", enums.Env.Port)))
}
